<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Exception;
use App\Feature;
use App\FeatureMedia;
use App\Video;
use App\Category;
use App\City;
use DB;
use Mail;
use FFMpeg;

class FeatureController extends Controller
{
    public function index()
    {
        $datas = Feature::with('categories')->get();

        return view('admin.vrcontent.index', ['datas' => $datas]);
    }

    public function create()
    {
        $category = Category::all();
        $cities = City::all();

        return view('admin.vrcontent.create', ['categories' => $category, 'cities' => $cities]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        $validatedData = $request->validate([
            'nama_tempat' => 'required',
            'kategori' => 'required',
            'wilayah' => 'required'
        ]);

        $vr = new Feature();
        $vr->category_id = 1;
        $vr->city_id = $data['wilayah'];
        $vr->name = $data['nama_tempat'];
        $vr->information = $data['informasi'];
        $vr->thumbnail = '';
        $vr->save();

        return redirect('/admin/vr-contents')->with('success', 'Successfully saved!');
    }

    public function edit($id)
    {
        $feature = Feature::find($id);
        $category = Category::all();
        $cities = City::all();

        return view('admin.vrcontent.edit', ['feature' => $feature, 'categories' => $category, 'cities' => $cities]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $validatedData = $request->validate([
            'nama_tempat' => 'required',
            'kategori' => 'required',
            'wilayah' => 'required'
        ]);

        $vr = Feature::find($id);
        $vr->category_id = 1;
        $vr->city_id = $data['wilayah'];
        $vr->name = $data['nama_tempat'];
        $vr->information = $data['informasi'];
        if (empty($vr->thumbnail)) $vr->thumbnail = '';
        $vr->save();

        return redirect('/admin/vr-contents')->with('success', 'Successfully saved!');
    }

    public function delete($id)
    {
        $vr = Feature::with('medias')->where('id', $id)->first();
        $medias = $vr->medias;
        
        foreach ($medias as $media) {
            if ($media->type == 1) {
                if (file_exists('uploads/images/' . $media->name))
                    unlink('uploads/images/' . $media->name);
            }
            else {
                if (file_exists('uploads/videos/' . $media->name))
                    unlink('uploads/videos/' . $media->name);
                if (file_exists('uploads/videos/thumbnails/' . $media->thumbnail))
                    unlink('uploads/videos/thumbnails/' . $media->thumbnail);
            }

            $media->delete();
        }
        
        $vr->delete();
        return redirect('/admin/vr-contents')->with('success', 'Successfully deleted!');
    }

    public function media($id)
    {
        $feature = Feature::find($id);

        return view('admin.vrcontent.media', ['feature' => $feature]);
    }

    public function mediaUploadVideo(Request $request ,$id)
    {
        $fileName = time() . "." . $request->file->getClientOriginalExtension();
        $request->file->move('uploads/videos', $fileName);

        $media = new FeatureMedia();
        $media->feature_id = $id;
        $media->name = $fileName;
        $media->cover = 0;
        $media->status = 0;
        $media->type = 0;
        $media->thumbnail = '';

        $status = $media->save();

        if ($status) {
            $thumbFolder = 'uploads/videos/thumbnails/';
            $thumbName = time() . '.jpg';

            if (!is_dir($thumbFolder))
                mkdir($thumbFolder, 0775);

            $ffmpeg = FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open('uploads/videos/'. $fileName);
            $video
                ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(10))
                ->save($thumbFolder.$thumbName);

            $media->thumbnail = $thumbName;
            $media->save();
        }

        return response('success', 200);
    }

    public function makeThumbnail()
    {
        $ffmpeg = FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open('uploads/videos/1601421966.mkv');
            $video
                ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(10))
                ->save('uploads/videos/thumbnails/frame1.jpg');
    }
    
    public function mediaUploadImage(Request $request ,$id)
    {
        $fileName = time() . "." . $request->file->getClientOriginalExtension();
        $type = 1;
        $request->file->move('uploads/images', $fileName);

        $media = new FeatureMedia();
        $media->feature_id = $id;
        $media->name = $fileName;
        $media->cover = 0;
        $media->status = 0;
        $media->type = 1;
        $media->thumbnail = '';

        $media->save();

        return response('success', 200);
    }

    public function mediaImagesAjax($id)
    {
        $images = DB::table('feature_medias')
            ->where(['feature_id' => $id, 'type' => 1])->get();

        $data = [];
        foreach ($images as $image) {
            $image->url = url('uploads/images/' . $image->name);
            $data[] = $image;
        }

        return response($data, 200);
    }
    
    public function mediaVideosAjax($id)
    {
        $images = DB::table('feature_medias')
            ->where(['feature_id' => $id, 'type' => 0])->get();

        $data = [];
        foreach ($images as $image) {
            $image->url = url('uploads/videos/' . $image->name);
            $image->thumbnail = url('uploads/videos/thumbnails/' . $image->thumbnail);

            $data[] = $image;
        }

        return response($data, 200);
    }

    public function mediaDelete($id)
    {
        $media = FeatureMedia::find($id);
        if ($media->type == 1) {
            if (file_exists('uploads/images/' . $media->name))
                unlink('uploads/images/' . $media->name);
        }
        else {
            if (file_exists('uploads/videos/' . $media->name))
                unlink('uploads/videos/' . $media->name);
            if (file_exists('uploads/videos/thumbnails/' . $media->thumbnail))
                unlink('uploads/videos/thumbnails/' . $media->thumbnail);
        }

        $media->delete();

        return response('success', 200);
    }

    public function mediaSetCover($id)
    {
        DB::table('feature_medias')
            ->update(['cover' => 0]);

        $media = FeatureMedia::find($id);
        $media->cover = 1;
        $media->save();

        return response('success', 200);
    }

    public function getFeature(Request $request)
    {
        $data = DB::table('features')
                            ->leftJoin('categories', 'features.category_id', '=', 'categories.id')
                            ->leftJoin('videos', function ($join) {
                                $join->on('features.id', '=', 'videos.feature_id')
                                    ->where('videos.cover', '=', 1);
                            })
                            ->select('features.id AS object_id', 'features.name AS object_name', 'features.information', 'categories.name AS category', 'videos.thumbnail')
                            ->where('category_id', $request->categoryId)
                            ->paginate(3);
        
        return $data;
    }

    public function sendImageToEmail(Request $request)
    {
        // return $_FILES['image'];
        $data = json_decode(request()->getContent(), true);

        $to_email = $request['email'];
        $file = $_FILES['file'];

        try{
            move_uploaded_file($file['tmp_name'], "../sistem-wahana-publik/php_mail/" . $file['name']);

            // preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            // $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            // $image = str_replace(' ', '+', $image);
            // $imageName = 'image_' . time() . '.' . 'png'; //generating unique file name;
            // Storage::disk('public')->put($_FILES['image']['name'], $_FILES['image']);
            // $file = Storage::disk('public')->get($imageName);

            Mail::send('emails.mail', [], function ($message) use ($to_email, $file) {
                $message->to($to_email)
                    ->subject('JCC Photo Booth');
                    
                $message->attach("../sistem-wahana-publik/php_mail/" . $file['name']);
                // $message->attachData($_FILES['image'], $_FILES['image']['name'], [
                //     'mime' => $_FILES['image']['type'],
                // ]);
                $message->from('wahanapublikccjabar@gmail.com', 'Jabar Command Center');
            });

            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => "Email sent successfully!"
            ]);

        } catch (Exception $e) {
            return $e->getMessage();
            return response()->json([
                'code' => 200,
                'success' => true,
                'message' => "Email sent successfully!"
            ]);
        }

    }

    public function testJson(Request $request)
    {
        $data = json_decode(request()->getContent(), true);
        return $data['image'];

    } 

    public function getCSRFToken(Request $request)
    {
        $thisToken['_token'] = csrf_token();
        return $thisToken;
    }

    public function getListVideoByFeatureId(Request $request, $featureId)
    {
        $result[] = Video::where('feature_id', $featureId)->first();
        $videos['videoList'] = $result;

        return $videos;
    } 
}
