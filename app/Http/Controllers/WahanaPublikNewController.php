<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WahanaPublikNewController extends Controller
{
    public function index()
    {
        return view('wahana-publik-new/index');
    }

    public function jabarDalamAngkaKategori()
    {
        return view('wahana-publik-new/sub-jda');
    }

    public function bidangGeografi()
    {
        return view('wahana-publik-new/jda-geo');
    }

    public function bidangPenduduk()
    {
        return view('wahana-publik-new/jda-mass');
    }

    public function bidangPemerintahan()
    {
        return view('wahana-publik-new/jda-gov');
    }
}

?>