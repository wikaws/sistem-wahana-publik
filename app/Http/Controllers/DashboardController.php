<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feature;
use App\GuestBookPersonal;
use App\GuestBookRombongan;

class DashboardController extends Controller
{
    public function index()
    {
        $features = Feature::count();
        $guestbookPersonal = GuestBookPersonal::count();
        $guestbookRombUmum = GuestBookRombongan::where('type', 'umum')->count();
        $guestbookRombInstansi = GuestBookRombongan::where('type', 'instansi')->count();

        $data = array(
            "vr-content" => $features,
            "guest-book-personal" => $guestbookPersonal,
            "guest-book-umum" => $guestbookRombUmum,
            "guest-book-instansi" => $guestbookRombInstansi
        );

        return view('admin.dashboard.dashboard', ['data' => $data]);
    }
}
