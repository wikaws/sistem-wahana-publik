<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Administrative;
use App\City;

class AdministrativeController extends Controller
{
    public function index()
    {
        $datas = Administrative::with('city')->get();
        // return $datas;

        return view('admin.pemerintahan.administratif.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.pemerintahan.administratif.create', ['cities' => $cities]);

    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'kecamatan' => 'required',
            'perdesaan' => 'required',
            'perkotaan' => 'required'
        ]);

        $admin = new Administrative();
        $admin->city_id = $request->kabupaten;
        $admin->year = $request->tahun;
        $admin->subdistrict = $request->kecamatan;
        $admin->rural = $request->perdesaan;
        $admin->urban = $request->perkotaan;
        $admin->save();

        return redirect('/admin/wilayah-administratif')->with(['success' => 'Data Successfully Added!']);

    }
}
