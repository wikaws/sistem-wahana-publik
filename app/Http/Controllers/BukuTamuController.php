<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BukuTamuController extends Controller
{
    public function getDataPersonal()
    {
        $data = DB::table('guest_book_personals')->get();

        return view('admin.buku_tamu.personal', ['datas' => $data]);
    }

    public function getDataGroupUmum()
    {
        $data = DB::table('guest_book_group')->where('type', 'umum')->get();

        return view('admin.buku_tamu.grup_umum', ['datas' => $data]);

    }

    public function getDataGroupInstansi()
    {
        $data = DB::table('guest_book_group')->where('type', 'instansi')->get();

        return view('admin.buku_tamu.grup_instansi', ['datas' => $data]);

    }
}
