<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employment;
use App\City;

class EmploymentController extends Controller
{
    public function index()
    {
        $datas = Employment::with('city')->get();
        // return $datas;

        return view('admin.ketenagakerjaan.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.ketenagakerjaan.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'bekerja' => 'required',
            'pengangguran' => 'required',
            'bukan_angkatan_kerja' => 'required'
        ]);

        $active = $request->bekerja + $request->pengangguran;

        $employment = new Employment();
        $employment->city_id = $request->kabupaten;
        $employment->year = $request->tahun;
        $employment->working = $request->bekerja;
        $employment->unemployment = $request->pengangguran;
        $employment->active = $active;
        $employment->inactive = $request->bukan_angkatan_kerja;
        $employment->save();

        return redirect('/admin/ketenagakerjaan')->with(['success' => 'Data Successfully Added!']);
    }
}
