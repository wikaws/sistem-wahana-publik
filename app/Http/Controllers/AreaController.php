<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\City;

class AreaController extends Controller
{
    public function index()
    {
        $datas = Area::with('city')->get();
        // return $datas;

        return view('admin.geografi.wilayah.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.geografi.wilayah.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'luas' => 'required',
            'persentase' => 'required'
        ]);

        $area = new Area();
        $area->city_id = $request->kabupaten;
        $area->year = $request->tahun;
        $area->total_area = $request->luas;
        $area->percentage = $request->persentase;
        $area->save();

        return redirect('/admin/luas-wilayah')->with(['success' => 'Data Successfully Added!']);
    }

    public function edit(Request $request, $id)
    {
        $datas = Area::find($id);
        $cities = City::all();

        // return $datas;
        return view('admin.geografi.wilayah.edit', ['datas' => $datas, 'cities' => $cities]);
    }
}
