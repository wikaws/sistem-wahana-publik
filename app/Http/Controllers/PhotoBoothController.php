<?php

namespace App\Http\Controllers;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoBoothController extends Controller
{
    private $api;
    public function __construct(Facebook $fb)
    {
        $this->middleware(function ($request, $next) use ($fb) {
            $fb->setDefaultAccessToken(Auth::user()->token);
            $this->api = $fb;
            return $next($request);
        });
    }

    public function postPhotoToFacebook(Request $request)
    {
        // $absolute_image_path = '/var/www/larave/storage/app/images/lorde.png';
        $absolute_image_path = Storage::url('IMG_0896.jpg');

        try {
            $response = $this->api->post('/me/feed', [
                'message' => 'this is test',
                'source'    =>  $this->api->fileToUpload('/path/to/file.jpg')
            ])->getGraphNode()->asArray();
    
            if($response['id']){
            // post created
                return $response['id'];
            }
        } catch (FacebookSDKException $e) {
            // dd($e); // handle exception
            return 'Failed!';
        }
        // return $id;
    }
}
