<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CivilServant;
use App\City;

class CivilServantController extends Controller
{
    public function index()
    {
        $datas = CivilServant::with('city')->get();
        // return $datas;

        return view('admin.pemerintahan.pns.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.pemerintahan.pns.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'laki_laki' => 'required',
            'perempuan' => 'required'
        ]);

        $pns = new CivilServant();
        $pns->city_id = $request->kabupaten;
        $pns->year = $request->tahun;
        $pns->male = $request->laki_laki;
        $pns->female = $request->perempuan;
        $pns->category = 0;
        $pns->save();

        return redirect('/admin/pns')->with(['success' => 'Data Successfully Added!']);

    }
}
