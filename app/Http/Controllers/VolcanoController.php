<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Volcano;

class VolcanoController extends Controller
{
    public function index()
    {
        $datas = Volcano::all();
        // return $datas;

        return view('admin.geografi.posisi.index', ['datas' => $datas]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_gunung' => 'required',
            'tahun' => 'required',
            'ls' => 'required',
            'bt' => 'required',
            'tinggi' => 'required'
        ]);

        $volcano = new Volcano();
        $volcano->mountain_name = $request->nama_gunung;
        $volcano->year = $request->tahun;
        $volcano->lintang_selatan = $request->ls;
        $volcano->bujur_timur = $request->bt;
        $volcano->height = $request->tinggi;
        $volcano->save();

        return redirect('/admin/posisi-geografi')->with(['success' => 'Data Successfully Added!']);


    }
}
