<?php

namespace App\Http\Controllers;
use DB;
use Area;
use Volcano;

use Illuminate\Http\Request;

class JabarDalamAngkaController extends Controller
{
    public function getDataGeography(Request $request, $year)
    {
        $luas = DB::table('areas')
                ->select(DB::raw('SUM(total_area) AS total_area, MIN(total_area) AS terkecil, MAX(total_area) AS terbesar'))
                ->where('year', $year)
                ->get();

        $kab = DB::table('cities')
                ->where('category', 1)
                ->count();

        $kota = DB::table('cities')
                ->where('category', 2)
                ->count();

        $total_gunung = DB::table('volcanoes')
                        ->count();

        $gunung_tertinggi = DB::table('volcanoes')
                            ->whereRaw('height = (SELECT MAX(height) FROM volcanoes)')
                            ->where('year', $year)
                            ->get();

        $data = array(
            "luas_jabar" => $luas[0]->total_area,
            "luas_terbesar" => $luas[0]->terbesar,
            "luas_terkecil" => $luas[0]->terkecil,
            "total_kabupaten" => $kab,
            "total_kota" => $kota,
            "total_gunung" => $total_gunung,
            "gunung_tertinggi" => array(
                "nama_gunung" => $gunung_tertinggi[0]->mountain_name,
                "tinggi" => $gunung_tertinggi[0]->height
            )
        );

        return $data;
    }

    public function getDataGovernment(Request $request, $year)
    {
        $pns = DB::table('civil_servants')
                ->select(DB::raw('SUM(male) AS total_male, SUM(female) AS total_female'))
                ->where('year', $year)
                ->get();

        $dprd = DB::table('representatives')
                ->select(DB::raw('SUM(male) AS total_male, SUM(female) AS total_female'))
                ->where('year', $year)
                ->get();

        $admin = DB::table('administratives')
                ->select(DB::raw('SUM(subdistrict) AS kecamatan, SUM(rural) AS perdesaan, SUM(urban) AS perkotaan'))
                ->where('year', $year)
                ->get();

        $data = array(
            "pns_pria" => $pns[0]->total_male,
            "pns_wanita" => $pns[0]->total_female,
            "dprd_pria" => $dprd[0]->total_male,
            "dprd_wanita" => $dprd[0]->total_female,
            "kecamatan" => $admin[0]->kecamatan,
            "perdesaan" => $admin[0]->perdesaan,
            "perkotaan" => $admin[0]->perkotaan
        );

        return $data;
    }

    public function getDataPopulationEmployement(Request $request, $year)
    {
        //total penduduk
        $total_penduduk = DB::table('populations')
                        ->where('year', $year)
                        ->sum('population');

        //kepadatan penduduk per km2
        $luas = DB::table('areas')
                        ->where('year', $year)
                        ->sum('total_area');
        $density = $total_penduduk/$luas;

        //jumlah penduduk terbesar
        $pend_terbesar = DB::table('populations')
                            ->join('cities', 'populations.city_id', '=', 'cities.id')
                            ->select('cities.category', 'cities.name', 'populations.population')
                            ->whereRaw('population = (SELECT MAX(population) FROM populations)')
                            ->get();

        //laju pertumbuhan penduduk
        $jumlah_penduduk_2016 = 46709570; //hardcode untuk sementara karena gak punya datanya
        $jumlah_penduduk_2017 = 48037830; //hardcode untuk sementara karena gak punya datanya
        $t = $year - ($year - 1);
        $growth_rate = round(((($jumlah_penduduk_2017/$jumlah_penduduk_2016)**(1/$t))-1)*100, 2);
        // return $growth_rate;

        //Rasio jenis kelamin
        $sex = DB::table('populations')
                ->selectRaw('SUM(male) AS total_male, SUM(female) AS total_female')
                ->where('year', $year)
                ->get();
        $sexratio = round(($sex[0]->total_male / $sex[0]->total_female) * 100, 2);

        //Tingkat pengangguran
        $employment = DB::table('employments')
                ->selectRaw('SUM(unemployment) AS total_unemployment, SUM(active) AS total_active, SUM(inactive) AS total_inactive')
                ->where('year', $year)
                ->get();

        $pengangguran = round(($employment[0]->total_unemployment/$employment[0]->total_active)*100, 2);
        $angkatan_kerja = $employment[0]->total_active;
        $partisipasi_kerja = round(($angkatan_kerja/($angkatan_kerja+$employment[0]->total_inactive))*100, 2);
        
        $data = array(
            "total_penduduk" => $total_penduduk,
            "kepadatan_penduduk" => $density,
            "jumlah_penduduk_terbesar" => array(
                "wilayah" => ($pend_terbesar[0]->category == 1 ? 'Kabupaten' : 'Kota').' '.$pend_terbesar[0]->name,
                "population" => $pend_terbesar[0]->population
            ),
            "laju_pertumbuhan_penduduk" => $growth_rate,
            "rasio_jenis_kelamin" => $sexratio,
            "tingkat_pengangguran" => $pengangguran,
            "jumlah_angkatan_kerja" => $angkatan_kerja,
            "tingkat_partisipasi_kerja" => $partisipasi_kerja,
        );

        return $data;
    }
}
