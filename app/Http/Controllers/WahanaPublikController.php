<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WahanaPublikController extends Controller
{
    public function index()
    {
        return view('wahana-publik/homepage');
    }

    public function jabarDalamAngka()
    {
        $data['title'] = "Jabar Dalam Angka ";
        $data['url'] = "sistem-wahana-publik/jabar-dalam-angka/kategori";
        $data['kategori'] = true;
        return view('wahana-publik/tahun',$data);
    }

    public function jabarDalamAngkaKategori($tahun)
    {
        $data['title'] = "Pilih Kategori Data ";
        $data['tahun'] = $tahun;
        return view('wahana-publik/jabar-angka/kategori',$data);
    }

    public function bidangGeografi($tahun)
    {
        $data['title'] = "Detail Data Bidang Geografi ";
        return view('wahana-publik/jabar-angka/geografi',$data);
    }

    public function bidangPenduduk($tahun)
    {
        $data['title'] = "Detail Data Bidang Penduduk dan ketenagakerjaan ";
        return view('wahana-publik/jabar-angka/penduduk',$data);
    }

    public function bidangPemerintahan($tahun)
    {
        $data['title'] = "Detail Data Bidang Pemerintahan ";
        return view('wahana-publik/jabar-angka/pemerintahan',$data);
    }

    public function grafikStatistik()
    {
        $data['title'] = "Grafik Dan Statistik ";
        $data['url'] = "sistem-wahana-publik/grafik-statistik";
        return view('wahana-publik/tahun',$data);
    }
    
    public function detailGrafikStatistik($tahun)
    {
        $data['title'] = "Detail Grafik & Statistik ";
        $data['url'] = "sistem-wahana-publik/grafik-statistik";
        return view('wahana-publik/grafik-statistik/kontent',$data);
    }

    public function proyekStrategis()
    {
        $data['title'] = "Proyek Strategis ";
        $data['url'] = "sistem-wahana-publik/proyek-strategis";
        return view('wahana-publik/tahun',$data);
    }
}
?>