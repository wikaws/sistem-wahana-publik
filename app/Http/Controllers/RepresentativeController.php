<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Representative;
use App\City;

class RepresentativeController extends Controller
{
    public function index()
    {
        $datas = Representative::with('city')->get();
        // return $datas;

        return view('admin.pemerintahan.dprd.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.pemerintahan.dprd.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'laki_laki' => 'required',
            'perempuan' => 'required'
        ]);

        $dprd = new Representative();
        $dprd->city_id = $request->kabupaten;
        $dprd->year = $request->tahun;
        $dprd->male = $request->laki_laki;
        $dprd->female = $request->perempuan;
        $dprd->category = 0;
        $dprd->political_parties = '';
        $dprd->save();

        return redirect('/admin/dprd')->with(['success' => 'Data Successfully Added!']);

    }
}
