<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\GuestBook;
use Carbon\Carbon;

class GuestBookController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('guest_book_group')->get();

        return $data;
    }

    public function addPersonal(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required|numeric',
            'address' => 'required',
            'age' => 'required',
            'job' => 'required',
            'image' => 'required'
        ]);

        //upload photo
        $folderPath = "images/";
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
    
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.jpg';
    
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);

        $insert = DB::table('guest_book_personals')->insert([
                    'name' => $request->name, 
                    'phone' => $request->phone, 
                    'email' => $request->email, 
                    'address' => $request->address, 
                    'age' => $request->age, 
                    'job' => $request->job,
                    'photo' => $fileName,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
        ]);

        return redirect('/guest-book')->with('success', 'Successfully saved!');
    }

    public function addGroup(Request $request)
    {
        if($request->type = 'umum'){
            $validatedData = $request->validate([
                'nama' => 'required|max:255',
                'email' => 'required|email:rfc,dns',
                'telp' => 'required|numeric',
                'alamat' => 'required',
                'image' => 'required'
            ]);
        }else{
            $validatedData = $request->validate([
                'nama2' => 'required|max:255',
                'email_instansi' => 'required|email:rfc,dns',
                'telp_instansi' => 'required|numeric',
                'instansi' => 'required',
                'total_pengunjung' => 'required',
                'keperluan' => 'required',
                'image' => 'required'
            ]);
        }

        //upload photo
        $folderPath = "images/";
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
    
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.jpg';
    
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);

        $type = $request->type == null ? $request->type_instansi : $request->type;
        $name = $request->nama == null ? $request->nama_instansi : $request->nama;
        $email = $request->email == null ? $request->email_instansi : $request->email;
        $phone = $request->telp == null ? $request->telp_instansi : $request->telp;

        $insert = DB::table('guest_book_group')->insert([
            'type' => $type,
            'name' => $name, 
            'phone' => $phone, 
            'email' => $email, 
            'address' => $request->alamat, 
            'visitor_age_1' => $request->umur_1,
            'visitor_age_2' => $request->umur_2,
            'visitor_age_3' => $request->umur_3,
            'visitor_age_4' => $request->umur_4,
            'visitor_age_5' => $request->umur_5,  
            'visitor_age_6' => $request->umur_6,
            'total_visitor' => $request->total_pengunjung,
            'agency' => $request->instansi,
            'purpose' => $request->keperluan,
            'photo' => $fileName,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return redirect('/guest-book')->with('success', 'Successfully saved!');
    }
}
