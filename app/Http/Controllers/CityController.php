<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class Citycontroller extends Controller
{
    public function index()
    {
        $datas = City::all();

        return view('admin.kabupaten.index', ['datas' => $datas]);
    }

    public function edit(Request $request, $id)
    {
        $datas = City::find($id);
        // return $datas;
        return view('admin.kabupaten.edit', ['datas' => $datas]);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'wilayah' => 'required',
            'kategori' => 'required'
        ]);

        $data = City::find($id);
        $data->name = $request->wilayah;
        $data->category = $request->kategori;
        $data->save();

        return redirect('/admin/kabupaten')->with(['success' => 'Data Successfully Updated!']);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'wilayah' => 'required',
            'kategori' => 'required'
        ]);

        $data = new City();
        $data->name = $request->wilayah;
        $data->category = $request->kategori;
        $data->save();

        return redirect('/admin/kabupaten')->with(['success' => 'Data Successfully Added!']);

    }
}
