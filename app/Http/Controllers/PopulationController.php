<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Population;
use App\Area;
use App\City;

class PopulationController extends Controller
{
    public function index()
    {
        $datas = Population::with('city')->get();
        // return $datas;

        return view('admin.kependudukan.index', ['datas' => $datas]);
    }

    public function create()
    {
        $cities = City::all();

        return view('admin.kependudukan.create', ['cities' => $cities]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kabupaten' => 'required',
            'tahun' => 'required',
            'laki_laki' => 'required',
            'perempuan' => 'required'
        ]);

        $total_population = $request->laki_laki + $request->perempuan;
        $sex_ratio = ($request->laki_laki/$request->perempuan)*100;

        $area = Area::where('city_id', $request->kabupaten)->where('year', $request->tahun)->first();
        $density = $total_population/$area->total_area;

        $population = new Population();
        $population->city_id = $request->kabupaten;
        $population->year = $request->tahun;
        $population->male = $request->laki_laki;
        $population->female = $request->perempuan;
        $population->population = $total_population;
        $population->sex_ratio = round($sex_ratio, 2);
        $population->population_density = round($density, 2);
        $population->save();

        return redirect('/admin/kependudukan')->with(['success' => 'Data Successfully Added!']);
    }
}
