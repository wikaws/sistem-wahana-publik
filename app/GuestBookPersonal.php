<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestBookPersonal extends Model
{
    protected $table = "guest_book_personals";
}
