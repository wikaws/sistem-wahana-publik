<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
}
