<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function medias()
    {
        return $this->hasMany('App\FeatureMedia');
    }
    
    public function categories()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
}
