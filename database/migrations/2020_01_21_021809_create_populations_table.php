<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('populations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->integer('city_id');
            $table->integer('year');
            $table->integer('population');
            $table->integer('population_density');
            $table->decimal('growth_rate', 8, 2);
            $table->decimal('percentage', 8, 2);
            $table->integer('male');
            $table->integer('female');
            $table->decimal('sex_ratio', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('populations');
    }
}
