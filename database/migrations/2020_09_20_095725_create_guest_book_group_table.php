<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestBookGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guest_book_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('name');
            $table->string('email');
            $table->integer('phone');
            $table->string('photo');
            $table->integer('visitor_age_1');
            $table->integer('visitor_age_2');
            $table->integer('visitor_age_3');
            $table->integer('visitor_age_4');
            $table->integer('visitor_age_5');
            $table->integer('visitor_age_6');
            $table->integer('total_visitor');
            $table->string('agency');
            $table->text('purpose');
            $table->text('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_book_group');
    }
}
