<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WahanaPublikNewController@index')->name('index-home');
Route::get('/jda/kategori', 'WahanaPublikNewController@jabarDalamAngkaKategori')->name('jda-kategori');
Route::get('/jda/data-geografi', 'WahanaPublikNewController@bidangGeografi')->name('jda-geo');
Route::get('/jda/data-pemerintahan', 'WahanaPublikNewController@bidangPemerintahan')->name('jda-gov');
Route::get('/jda/data-kependudukan-dan-ketenagakerjaan', 'WahanaPublikNewController@bidangPenduduk')->name('jda-mass');
// Route::get('/', 'WahanaPublikController@index');
// Route::get('/jabar-dalam-angka', 'WahanaPublikController@jabarDalamAngka')->name('jabar-dalam-angka');
// Route::get('/jabar-dalam-angka/kategori/{tahun}', 'WahanaPublikController@jabarDalamAngkaKategori')->name('jabar-dalam-angka-kategori');
// Route::get('/jabar-dalam-angka/data-pemerintahan/{tahun}', 'WahanaPublikController@bidangPemerintahan')->name('bidang-pemerintahan');
// Route::get('/jabar-dalam-angka/data-geografi-dan-iklim/{tahun}', 'WahanaPublikController@bidangGeografi')->name('bidang-geografi');
// Route::get('/jabar-dalam-angka/data-kependudukan-dan-ketenagakerjaan/{tahun}', 'WahanaPublikController@bidangPenduduk')->name('bidang-penduduk');
// Route::get('/grafik-statistik', 'WahanaPublikController@grafikStatistik')->name('grafik-statistik');
// Route::get('/grafik-statistik/{tahun}', 'WahanaPublikController@detailGrafikStatistik')->name('detail-grafik-statistik');
// Route::get('/proyek-strategis', 'WahanaPublikController@proyekStrategis')->name('proyek-strategis');
Route::get('/interactive', function () {
    return view('interactive/interactive');
});

//============= API for VR Content ================================
Route::get('get-features/{categoryId}', 'FeatureController@getFeature');
Route::get('get-contents-by-id/{featureId}', 'FeatureController@getListVideoByFeatureId');
Route::post('sendToEmail', 'FeatureController@sendImageToEmail');
Route::post('/test-json', 'FeatureController@testJson');
Route::get('/generate-token', 'FeatureController@getCSRFToken');

//============= API for Guest Book ================================
Route::get('guest-book', function () {
    return view('guestbook/index');
});
Route::get('guest-book/personal', function () {
    return view('guestbook/individu');
});
Route::get('guest-book/group', function () {
    return view('guestbook/group');
});
Route::post('guest-book/add-personal', 'GuestBookController@addPersonal');
Route::post('guest-book/add-group', 'GuestBookController@addGroup');

//============= API for Jabar Dalam Angka =========================
Route::get('/data-geografi/{year}', 'JabarDalamAngkaController@getDataGeography');
Route::get('/data-pemerintahan/{year}', 'JabarDalamAngkaController@getDataGovernment');
Route::get('/data-penduduk-dan-ketenagakerjaan/{year}', 'JabarDalamAngkaController@getDataPopulationEmployement');

Auth::routes();
Route::get('/admin', 'Auth\LoginController@showFormLogin')->name('admin');
Route::get('/admin/register', 'Auth\RegisterController@showFormRegister')->name('admin.register');
Route::get('/admin/password/reset', 'Auth\ResetPasswordController@showFormPasswordEmail')->name('admin.password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showFormPasswordReset')->name('admin.password.reset');

// Route::post('/admin/login', 'Auth\LoginController@login');

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('kabupaten', 'CityController@index');
    Route::get('kabupaten/create', function() {
        return view('admin.kabupaten.create');
    });
    Route::get('kabupaten/edit/{id}', 'CityController@edit');
    Route::post('kabupaten/update/{id}', 'CityController@update');
    Route::post('kabupaten/store', 'CityController@store');

    Route::get('luas-wilayah', 'AreaController@index');
    Route::get('luas-wilayah/create', 'AreaController@create');
    Route::post('luas-wilayah/store', 'AreaController@store');

    Route::get('posisi-geografi', 'VolcanoController@index');
    Route::get('posisi-geografi/create', function() {
        return view('admin.geografi.posisi.create');
    });
    Route::post('posisi-geografi/store', 'VolcanoController@store');

    Route::get('wilayah-administratif', 'AdministrativeController@index');
    Route::get('wilayah-administratif/create', 'AdministrativeController@create');
    Route::post('wilayah-administratif/store', 'AdministrativeController@store');

    Route::get('dprd', 'RepresentativeController@index');
    Route::get('dprd/create', 'RepresentativeController@create');
    Route::post('dprd/store', 'RepresentativeController@store');

    Route::get('pns', 'CivilServantController@index');
    Route::get('pns/create', 'CivilServantController@create');
    Route::post('pns/store', 'CivilServantController@store');

    Route::get('kependudukan', 'PopulationController@index');
    Route::get('kependudukan/create', 'PopulationController@create');
    Route::post('kependudukan/store', 'PopulationController@store');

    Route::get('ketenagakerjaan', 'EmploymentController@index');
    Route::get('ketenagakerjaan/create', 'EmploymentController@create');
    Route::post('ketenagakerjaan/store', 'EmploymentController@store');

    Route::get('vr-contents', 'FeatureController@index');
    Route::get('vr-content/create', 'FeatureController@create');
    Route::post('vr-content/store', 'FeatureController@store');
    Route::get('vr-content/edit/{id}', 'FeatureController@edit');
    Route::post('vr-content/update/{id}', 'FeatureController@update');
    Route::get('vr-content/delete/{id}', 'FeatureController@delete');
    Route::get('vr-content/media-images/{id}', 'FeatureController@mediaImagesAjax');
    Route::get('vr-content/media-videos/{id}', 'FeatureController@mediaVideosAjax');
    Route::get('vr-content/media/{id}', 'FeatureController@media');

    Route::post('vr-content/media-upload-video/{id}', 'FeatureController@mediaUploadVideo');
    Route::post('vr-content/media-upload-image/{id}', 'FeatureController@mediaUploadImage');
    Route::get('vr-content/media-delete/{id}', 'FeatureController@mediaDelete');
    Route::get('vr-content/media-set-cover/{id}', 'FeatureController@mediaSetCover');

    Route::get('guest-book/perorangan', 'BukuTamuController@getDataPersonal');
    Route::get('guest-book/rombongan-umum', 'BukuTamuController@getDataGroupUmum');
    Route::get('guest-book/rombongan-instansi', 'BukuTamuController@getDataGroupInstansi');

    // Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});


