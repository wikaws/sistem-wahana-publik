<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('wahana-publik/index');
});
Route::get('/grafik-statistik', function () {
    return view('wahana-publik/grafik-statistik');
})->name('grafik');
Route::get('/jabar-dalam-angka', function () {
    return view('wahana-publik/jabar-angka');
})->name('jabar_angka');

Route::get('/proyek-strategis', function () {
    return view('wahana-publik/proyek-strategis');
})->name('proyek_strategis');

//============= API for VR Content ================================
Route::get('get-features/{categoryId}', 'FeatureController@getFeature');
Route::get('get-feature-contents/{featureId}', 'FeatureController@getListVideoByFeatureId');
Route::post('sendToEmail', 'FeatureController@sendImageToEmail');
Route::post('/test-json', 'FeatureController@testJson');
Route::get('/generate-token', 'FeatureController@getCSRFToken');

//============= API for Photo Booth ===============================
Route::get('/post-photo-to-facebook', 'PhotoBoothController@postPhotoToFacebook');

//============= API for Guest Book ================================
Route::get('guest-book', function () {
    return view('guestbook/index');
});
Route::get('guest-book/personal', function () {
    return view('guestbook/individu');
});
Route::get('guest-book/group', function () {
    return view('guestbook/group');
});
Route::post('guest-book/add-personal', 'GuestBookController@addPersonal');
Route::post('guest-book/add-group', 'GuestBookController@addGroup');

//============= API for Jabar Dalam Angka =========================
Route::get('/data-geografi/{year}', 'JabarDalamAngkaController@getDataGeography');
Route::get('/data-pemerintahan/{year}', 'JabarDalamAngkaController@getDataGovernment');
Route::get('/data-penduduk-dan-ketenagakerjaan/{year}', 'JabarDalamAngkaController@getDataPopulationEmployement');

Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return view('admin.login');
    });

    Route::get('kabupaten', 'CityController@index');
    Route::get('kabupaten/create', function() {
        return view('admin.kabupaten.create');
    });
    Route::get('kabupaten/edit/{id}', 'CityController@edit');
    Route::post('kabupaten/update/{id}', 'CityController@update');
    Route::post('kabupaten/store', 'CityController@store');

    Route::get('luas-wilayah', 'AreaController@index');
    Route::get('luas-wilayah/create', 'AreaController@create');
    Route::post('luas-wilayah/store', 'AreaController@store');

    Route::get('posisi-geografi', 'VolcanoController@index');
    Route::get('posisi-geografi/create', function() {
        return view('admin.geografi.posisi.create');
    });
    Route::post('posisi-geografi/store', 'VolcanoController@store');

    Route::get('wilayah-administratif', 'AdministrativeController@index');
    Route::get('wilayah-administratif/create', 'AdministrativeController@create');
    Route::post('wilayah-administratif/store', 'AdministrativeController@store');

    Route::get('dprd', 'RepresentativeController@index');
    Route::get('dprd/create', 'RepresentativeController@create');
    Route::post('dprd/store', 'RepresentativeController@store');

    Route::get('pns', 'CivilServantController@index');
    Route::get('pns/create', 'CivilServantController@create');
    Route::post('pns/store', 'CivilServantController@store');

    Route::get('kependudukan', 'PopulationController@index');
    Route::get('kependudukan/create', 'PopulationController@create');
    Route::post('kependudukan/store', 'PopulationController@store');

    Route::get('ketenagakerjaan', 'EmploymentController@index');
    Route::get('ketenagakerjaan/create', 'EmploymentController@create');
    Route::post('ketenagakerjaan/store', 'EmploymentController@store');

    Route::get('vr-contents', 'FeatureController@index');
    Route::get('vr-content/create', 'FeatureController@create');
    Route::post('vr-content/store', 'FeatureController@store');

    Route::get('guest-book/perorangan', 'BukuTamuController@getDataPersonal');
    Route::get('guest-book/rombongan-umum', 'BukuTamuController@getDataGroupUmum');
    Route::get('guest-book/rombongan-instansi', 'BukuTamuController@getDataGroupInstansi');

    Route::get('home', function () {
        return view('admin.dashboard.dashboard');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
