-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 26, 2019 at 04:45 PM
-- Server version: 10.2.30-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wolfpupl_sistem_wahana_publik`
--

-- --------------------------------------------------------

--
-- Table structure for table `administratives`
--

CREATE TABLE `administratives` (
  `id` int(11) UNSIGNED NOT NULL,
  `year` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `rural` int(11) DEFAULT NULL,
  `urban` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administratives`
--

INSERT INTO `administratives` (`id`, `year`, `city_id`, `subdistrict`, `rural`, `urban`, `created_at`, `updated_at`) VALUES
(1, 2017, 1, 40, 143, 292, '2019-12-12 01:12:59', '2019-12-12 01:12:59');

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) UNSIGNED NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `total_area` decimal(18,2) DEFAULT NULL,
  `percentage` decimal(18,2) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `city_id`, `total_area`, `percentage`, `year`, `created_at`, `updated_at`) VALUES
(1, 1, 2710.62, 7.66, 2017, '2019-12-11 23:02:43', '2019-12-11 23:02:43'),
(2, 2, 4145.70, 11.72, 2017, '2019-12-11 23:12:50', '2019-12-11 23:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Objek Wisata'),
(2, 'Projek Strategis'),
(3, 'Atraksi Tradisional');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `category` smallint(2) DEFAULT NULL COMMENT '1 = Kabupaten, 2 = Kota',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Bogor', 1, '2019-12-12 05:13:27', '2019-12-12 16:13:57'),
(2, 'Sukabumi', 1, '2019-12-12 05:14:04', '2019-12-12 16:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `civil_servants`
--

CREATE TABLE `civil_servants` (
  `id` int(11) UNSIGNED NOT NULL,
  `category` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `male` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `civil_servants`
--

INSERT INTO `civil_servants` (`id`, `category`, `city_id`, `year`, `male`, `female`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2017, 9368, 8751, '2019-12-12 01:49:00', '2019-12-12 01:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `employments`
--

CREATE TABLE `employments` (
  `id` int(11) UNSIGNED NOT NULL,
  `year` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `working` int(11) DEFAULT NULL,
  `unemployment` int(11) DEFAULT NULL,
  `inactive` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL COMMENT '=working+unemployment',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employments`
--

INSERT INTO `employments` (`id`, `year`, `city_id`, `working`, `unemployment`, `inactive`, `active`, `created_at`, `updated_at`) VALUES
(1, 2017, 1, 2351753, 248368, 2600121, 2600121, '2019-12-12 05:37:22', '2019-12-12 05:37:22');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `information` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `category_id`, `name`, `information`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pangandaran', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2019-12-07 11:34:42', '2019-12-07 11:34:42'),
(2, 1, 'Geopark Ciletuh', 'Geopark Ciletuh terletak di Kabupaten Sukabumi, Jawa Barat. Disebut-sebut, tempat wisata ini surga tersembunyi di sukabumi.', '2019-12-19 04:10:01', '2019-12-19 04:10:01'),
(3, 1, 'Kawah Putih', 'Kawah Putih adalah sebuah tempat wisata di Jawa Barat yang terletak di desa Alam Endah, Kecamatan Ciwidey, Kabupaten Bandung Jawa Barat yang terletak di kaki Gunung Patuha. Kawah putih merupakan sebuah danau yang terbentuk dari letusan Gunung Patuha.', '2019-12-19 04:11:31', '2019-12-19 04:11:31'),
(4, 2, 'Pantai Pangandaran', 'Pantai Pangandaran merupakan sebuah objek wisata andalan Kabupaten Pangandaran yang terletak di sebelah tenggara Jawa Barat, tepatnya di Desa Pangandaran, Kecamatan Pangandaran, Kabupaten Pangandaran, Provinsi Jawa Barat.', '2019-12-19 04:18:28', '2019-12-19 04:18:28'),
(5, 2, 'Situ Ciburuy', 'Situ Ciburuy merupakan danau alami yang berada di sebelah barat Kota Bandung, tepatnya di Desa Ciburuy, Kecamatan Padalarang.', '2019-12-19 04:19:20', '2019-12-19 04:19:20'),
(6, 2, 'Gunung Padang', 'Situs Gunung Padang merupakan situs prasejarah peninggalan kebudayaan Megalitikum di Jawa Barat. Tepatnya berada di perbatasan Dusun Gunungpadang dan Panggulan, Desa Karyamukti, Kecamatan Campaka, Kabupaten Cianjur.', '2019-12-19 04:20:21', '2019-12-19 04:20:21'),
(7, 3, 'Tari Topeng Cirebon', 'Tari topeng Cirebon adalah salah satu tarian di wilayah kesultanan Cirebon. Tari Topeng Cirebon, kesenian ini merupakan kesenian asli daerah Cirebon, termasuk Subang, Indramayu, Jatibarang, Majalengka, Losari, dan Brebes. Disebut tari topeng karena penarinya menggunakan topeng di saat menari.', '2019-12-19 04:23:57', '2019-12-19 04:23:57'),
(8, 3, 'Wayang Golek', 'Wayang golek merupakan salah satu dari ragam kesenian wayang yang terbuat dari bahan kayu yang merupakan hasil perkembangan wayang kulit dari keterbatasan waktu supaya dapat ditampilkan pada siang atau malam hari. Pertama kali diperkenalkan oleh Sunan Kudus di daerah Kudus, Cirebon lalu Parahyangan.', '2019-12-19 04:25:22', '2019-12-19 04:25:22'),
(9, 3, 'Pencak Silat', 'Pencak silat atau silat adalah suatu seni bela diri tradisional yang berasal dari Kepulauan Nusantara. Seni bela diri ini secara luas dikenal di Indonesia, Malaysia, Brunei, dan Singapura, Filipina selatan, dan Thailand selatan sesuai dengan penyebaran berbagai suku bangsa Nusantara.', '2019-12-19 04:28:17', '2019-12-19 04:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `guest_books`
--

CREATE TABLE `guest_books` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guest_books`
--

INSERT INTO `guest_books` (`id`, `name`, `email`, `phone`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'test', 'it@octagonstudio.com', '089694385796', NULL, '2019-12-07 06:50:01', '2019-12-07 06:50:01'),
(2, 'test', 'it@octagonstudio.com', '089694385796', NULL, '2019-12-09 09:42:31', '2019-12-09 09:42:31'),
(3, 'john', 'john.doe@example.com', '089694385796', '5df65b46c0f4c.jpg', '2019-12-15 09:11:50', '2019-12-15 09:11:50'),
(4, 'Wita Karwila', 'witakarwila28@gmail.com', '089694385796', '5df65d5fc82a1.jpg', '2019-12-15 09:20:47', '2019-12-15 09:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `populations`
--

CREATE TABLE `populations` (
  `id` int(11) UNSIGNED NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `population_density` int(11) DEFAULT NULL COMMENT 'kepadatan penduduk per km2',
  `growth_rate` decimal(18,2) DEFAULT NULL COMMENT 'laju pertumbuhan pertahun',
  `percentage` decimal(18,2) DEFAULT NULL COMMENT 'persentse penduduk',
  `male` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `sex_ratio` decimal(18,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `populations`
--

INSERT INTO `populations` (`id`, `category`, `city_id`, `year`, `population`, `population_density`, `growth_rate`, `percentage`, `male`, `female`, `sex_ratio`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2017, 5715009, 2108, NULL, NULL, 2920288, 2794721, 104.49, '2019-12-12 04:42:53', '2019-12-12 04:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `representatives`
--

CREATE TABLE `representatives` (
  `id` int(11) UNSIGNED NOT NULL,
  `category` int(11) DEFAULT NULL COMMENT '1= kabupaten, 2= partai politik',
  `year` int(11) DEFAULT NULL,
  `male` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `political_parties` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `representatives`
--

INSERT INTO `representatives` (`id`, `category`, `year`, `male`, `city_id`, `female`, `political_parties`, `created_at`, `updated_at`) VALUES
(1, NULL, 2017, 44, 1, 6, NULL, '2019-12-12 01:37:10', '2019-12-12 01:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) UNSIGNED NOT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cover` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `feature_id`, `name`, `cover`, `thumbnail`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'video_1.mp4', 1, NULL, 0, '2019-12-07 10:38:10', '2019-12-07 10:37:52'),
(2, 1, 'video_2.mp4', 0, NULL, 0, '2019-12-07 10:38:09', '2019-12-07 10:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `volcanoes`
--

CREATE TABLE `volcanoes` (
  `id` int(11) UNSIGNED NOT NULL,
  `mountain_name` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `lintang_selatan` varchar(255) DEFAULT NULL,
  `bujur_timur` varchar(255) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volcanoes`
--

INSERT INTO `volcanoes` (`id`, `mountain_name`, `year`, `lintang_selatan`, `bujur_timur`, `height`, `created_at`, `updated_at`) VALUES
(1, 'Karang', 2017, '6° 60\'', '106° 02\'', 1778, '2019-12-12 00:15:16', '2019-12-12 00:15:16'),
(2, 'Kiara Beres-Gagak', 2017, '6° 43\'', '106° 39\'', 1511, '2019-12-12 00:16:52', '2019-12-12 00:16:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administratives`
--
ALTER TABLE `administratives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `civil_servants`
--
ALTER TABLE `civil_servants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employments`
--
ALTER TABLE `employments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest_books`
--
ALTER TABLE `guest_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `populations`
--
ALTER TABLE `populations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representatives`
--
ALTER TABLE `representatives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `volcanoes`
--
ALTER TABLE `volcanoes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administratives`
--
ALTER TABLE `administratives`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `civil_servants`
--
ALTER TABLE `civil_servants`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employments`
--
ALTER TABLE `employments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `guest_books`
--
ALTER TABLE `guest_books`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `populations`
--
ALTER TABLE `populations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `representatives`
--
ALTER TABLE `representatives`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `volcanoes`
--
ALTER TABLE `volcanoes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
