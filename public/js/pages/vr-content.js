$(function () {
  var base_url = $('.content-wrapper').data('url')

  $('.delete-row').on('click', function(e) {
    e.preventDefault()

    if (confirm('Apakah anda yakin menghapus data ini?')) {
      window.location.href = $(this).attr('url')
    }
  })

  function getImages() {
    var url = $('#photos').data('url')

    $.get(url, {}, function (images) {
      images = images.map(function (image) {
        return `<div class="col-md-2 mt-3 photo" data-id="${image.id}">
              <img src="${image.url}" class="img-thumbnail">
              <div class="buttons mt-2 d-flex">
                <a href="javascript:;" class="btn btn-xs flex-grow-1 ${(image.cover == 1 ? 'btn-default disabled': 'btn-primary set-to-cover')}">${(image.cover == 1 ? 'Sampul': 'Jadikan sampul')}</a>
                <a href="javascript:;" class="btn btn-xs btn-danger ml-3 delete px-4"><i class="fas fa-trash"></i></a>
              </div>
            </div>`
      })

      $('#photos .row').html(images.join('\n'))
    })
  }

  function getVideos() {
    var url = $('#videos').data('url')

    $.get(url, function (videos) {
      videos = videos.map(function (video) {
        return `<div class="col-md-2 mt-3 video" data-id="${video.id}">
          <img src="${video.thumbnail}" class="img-thumbnail">
          <div class="buttons mt-2 d-flex">
            <a href="javascript:;" class="btn btn-xs  flex-grow-1 ${(video.cover == 1 ? 'btn-default disabled': 'btn-primary set-to-cover')}">${(video.cover == 1 ? 'Sampul': 'Jadikan sampul')}</a>
            <a href="javascript:;" class="btn btn-xs btn-danger ml-3 delete px-4"><i class="fas fa-trash"></i></a>
          </div>
        </div>`
      })

      $('#videos .row').html(videos.join('\n'))
    })
  }

  getImages()
  getVideos()

  $('#photos').on('change', function () {
    getImages()
  })

  $('#videos').on('change', function () {
    getVideos()
  })

  $(document).on('click', '.photo .delete', function () {
    if (confirm("Apakah anda yakin menghapus foto ini?")) {
      let id = $(this).closest('.photo').data('id')
      let url = base_url + '/admin/vr-content/media-delete/' + id;
  
      $.get(url, function () {
        getImages()
      })
    }
  })
  
  $(document).on('click', '.video .delete', function () {
    if (confirm("Apakah anda yakin menghapus video ini?")) {
      let id = $(this).closest('.video').data('id')
      let url = base_url + '/admin/vr-content/media-delete/' + id;
  
      $.get(url, function () {
        getVideos()
      })
    }
  })

  $(document).on('click', '.set-to-cover', function(){
    let id = $(this).closest('div.col-md-2').data('id')
    let url = base_url + '/admin/vr-content/media-set-cover/' + id;

    $.get(url, function () {
      getVideos()
      getImages()
    })
  })
})