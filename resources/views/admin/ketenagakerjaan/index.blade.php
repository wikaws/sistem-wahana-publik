@extends('admin.app')

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show">
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@section('content')
  <div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KETENAGAKERJAAN DI JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Ketenagakerjaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <a href="{{ url('admin/ketenagakerjaan/create') }}" class="btn btn-success btn-lg" role="button">+ Add Data</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kabupaten/Kota</th>
                  <th>Tahun</th>
                  <th>Bekerja</th>
                  <th>Pengangguran Terbuka</th>
                  <th>Jumlah Angkatan Kerja<br>(Bekerja + Pengangguran)</th>
                  <th>Bukan Angkatan Kerja</th>
                  <th>Jumlah</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{ $data->city->name }}</td>
                        <td>{{ $data->year }}</td>
                        <td>{{ $data->working }}</td>
                        <td>{{ $data->unemployment }}</td>
                        <td>{{ $data->active }}</td>
                        <td>{{ $data->inactive }}</td>
                        <td>{{ $data->active + $data->inactive }}</td>
                        <td>
                            <a href="/admin/ketenagakerjaan/edit/{{ $data->id }}" class="btn btn-default"><i class="fas fa-edit"></i></a> | 
                            <a href="/admin/ketenagakerjaan/delete/{{ $data->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" style="text-align:right">JAWA BARAT</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('page-script')
<!-- page script -->
<script>
  $(function () {
    $("#dataTable").DataTable({
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total Bekerja over all pages
            total1 = api.column( 2 ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            
            total2 = api.column( 3 ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total3 = api.column( 4 ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total4 = api.column( 5 ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total5 = api.column( 6 ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total Bekerja over this page
            pageTotal1 = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            pageTotal2 = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            pageTotal3 = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            pageTotal4 = api.column( 5, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            pageTotal5 = api.column( 6, { page: 'current'} ).data().reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            
 
            // Update footer
            $( api.column( 2 ).footer() ).html(
                total1
            );
            $( api.column( 3 ).footer() ).html(
                total2
            );
            $( api.column( 4 ).footer() ).html(
                total3
            );
            $( api.column( 5 ).footer() ).html(
                total4
            );
            $( api.column( 6 ).footer() ).html(
                total5
            );
        }
    });
  });
</script>
@endsection