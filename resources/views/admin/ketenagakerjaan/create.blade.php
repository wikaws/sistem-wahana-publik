@extends('admin.app')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KETENAGAKERJAAN DI JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Ketenagakerjaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <form class="form-horizontal" action="/admin/ketenagakerjaan/store" method="POST">
        <div class="card">
            <h5 class="card-header">
              Tambah Data Baru
            </h5>
            <div class="card-body">
                @csrf
                <div class="form-group row">
                  <label for="formKabupaten" class="col-sm-2 col-form-label">Kabupaten/Kota</label>
                  <div class="col-sm-10">
                    <select id="formKabupaten" name="kabupaten" class="form-control">
                      <option value="" selected disabled>-- Pilih Kabupaten/Kota --</option>
                      @foreach($cities as $city)
                        <option value="{{ $city->id }}"> @if($city->category == 1) Kabupaten @else Kota @endif {{$city->name }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('kabupaten'))
                        <div class="text-danger"><small>{{ $errors->first('kabupaten') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formTahun" class="col-sm-2 col-form-label">Tahun</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formTahun" name="tahun">
                    @if($errors->has('tahun'))
                        <div class="text-danger"><small>{{ $errors->first('tahun') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formBekerja" class="col-sm-2 col-form-label">Bekerja</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formBekerja" name="bekerja">
                    @if($errors->has('bekerja'))
                        <div class="text-danger"><small>{{ $errors->first('bekerja') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formPengangguran" class="col-sm-2 col-form-label">Pengangguran</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formPengangguran" name="pengangguran">
                    @if($errors->has('pengangguran'))
                        <div class="text-danger"><small>{{ $errors->first('pengangguran') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formBukanAngkatanKerja" class="col-sm-2 col-form-label">Bukan Angkatan Kerja</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formBukanAngkatanKerja" name="bukan_angkatan_kerja">
                    @if($errors->has('bukan_angkatan_kerja'))
                        <div class="text-danger"><small>{{ $errors->first('bukan_angkatan_kerja') }}</small></div>
                    @endif
                  </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-danger margin-r-5" href="/admin/luas-wilayah">Cancel</a>
                    <button id="btn-save" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
      </form>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection