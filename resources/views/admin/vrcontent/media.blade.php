@extends('admin.app')

@section('page-css')
<link rel="stylesheet" href="{{ url('js/plugins/dropzone-5.7.0/dist/dropzone.css') }}">
<script src="{{ url('js/plugins/dropzone-5.7.0/dist/dropzone.js') }}"></script>

<script>
  Dropzone.options.myAwesomeDropzone = {
    timeout: 200000,
    init: function () {
      self = this

      this.on('complete', function (file) {
        self.removeFile(file)

        document.getElementById('videos').dispatchEvent(new Event('change'));
      })
    }
  };
  
  Dropzone.options.dropzoneImage = {
    init: function () {     
      self = this

      this.on('complete', function (file) {
        self.removeFile(file)

        document.getElementById('photos').dispatchEvent(new Event('change'));
      })
    }
  };
</script>
@endsection

@section('page-script')
<script src="{{ url('js/pages/vr-content.js') }}"></script>  
@endsection

@section('content')
<div class="content-wrapper" data-url="{{ url('') }}">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>VR CONTENTS</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
            <li class="breadcrumb-item active">VR Contents</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="card">
      <h5 class="card-header">
        Video
      </h5>
      
      <div class="card-body">
        <form action="{{ url('admin/vr-content/media-upload-video/'. $feature->id) }}" class="dropzone"
          id="my-awesome-dropzone">
          @csrf
          <div class="fallback">
            <input name="file" type="file" />
          </div>
        </form>

        <div id="videos" class="mt-3" data-url="{{ url('admin/vr-content/media-videos/'. $feature->id)}}">
          <div class="row">
            {{-- <div class="col-md-2">
              <img src="{{ url('uploads/images/1601401249.jpg') }}" class="img-thumbnail">
              <div class="buttons mt-2 d-flex">
                <a href="#" class="btn btn-primary flex-grow-1">Set to cover</a>
                <a href="#" class="btn btn-danger ml-3"><i class="fas fa-trash"></i></a>
              </div>
            </div> --}}
          </div>
        </div>
        <!-- /#videos -->
      </div>

    </div>
    <!-- /.card -->

    <!-- Default box -->
    <div class="card">
      <h5 class="card-header">
        Photo
      </h5>
      
      <div class="card-body">
        <form action="{{ url('admin/vr-content/media-upload-image/'. $feature->id) }}" class="dropzone"
          id="dropzone-image">
          @csrf
          <div class="fallback">
            <input name="file" type="file" />
          </div>
        </form>
        <div id="photos" class="mt-3" data-url="{{ url('admin/vr-content/media-images/'. $feature->id)}}">
          <div class="row">
            {{-- <div class="col-md-2">
              <img src="{{ url('uploads/images/1601401249.jpg') }}" class="img-thumbnail">
              <div class="buttons mt-2 d-flex">
                <a href="#" class="btn btn-primary flex-grow-1">Set to cover</a>
                <a href="#" class="btn btn-danger ml-3"><i class="fas fa-trash"></i></a>
              </div>
            </div> --}}
          </div>
        </div>
        <!-- /#photos -->
        
      </div>
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
@endsection