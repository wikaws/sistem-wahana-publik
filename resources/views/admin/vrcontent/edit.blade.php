@extends('admin.app')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>VR CONTENTS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item active">VR Contents</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <form class="form-horizontal" action="{{ url('admin/vr-content/update/'. $feature->id) }}" method="POST" enctype="multipart/form-data">
        <div class="card">
            <h5 class="card-header">
              Edit Data
            </h5>
            <div class="card-body">
                @csrf
                <div class="form-group row">
                  <label for="formWilayah" class="col-sm-2 col-form-label">Kabupaten/Kota</label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control" id="formWilayah" name="wilayah"> -->
                    <select class="form-control" name="wilayah" id="formWilayah">
                      <option value="">-- Pilih Wilayah --</option>
                      @foreach($cities as $city)
                      <option value="{{ $city->id }}" @if($feature->id == $city->id) selected @endif>{{ $city->category == 1 ? 'Kabupaten' : 'Kota' }} {{ $city->name }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('wilayah'))
                        <div class="text-danger"><small>{{ $errors->first('wilayah') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formNamaTempat" class="col-sm-2 col-form-label">Nama Tempat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formNamaTempat" name="nama_tempat" value="{{ $feature->name }}">
                    @if($errors->has('nama_tempat'))
                        <div class="text-danger"><small>{{ $errors->first('nama_tempat') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formKategori" class="col-sm-2 col-form-label">Kategori</label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control" id="formKategori" name="kategori"> -->
                    <select class="form-control" name="kategori" id="formKategori">
                      <option value="">-- Pilih Kategori --</option>
                      @foreach($categories as $cat)
                      <option value="{{ $cat->id }}" @if($feature->category_id == $cat->id) selected @endif>{{ $cat->name }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('kategori'))
                        <div class="text-danger"><small>{{ $errors->first('kategori') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formInformasi" class="col-sm-2 col-form-label">Informasi</label>
                  <div class="col-sm-10">
                    <textarea name="informasi" id="informasi" class="form-control" cols="30" rows="4">{{ $feature->information }}</textarea>
                    @if($errors->has('informasi'))
                      <div class="text-danger"><small>{{ $errors->first('informasi') }}</small></div>
                    @endif
                  </div>
                </div>
            </div>
            
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-danger margin-r-5" href="{{ url('admin/vr-contents') }}">Cancel</a>
                    <button id="btn-save" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
      </form>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection