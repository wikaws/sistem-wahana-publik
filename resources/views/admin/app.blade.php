<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Sistem Wahana Publik</title>

   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{url('bower_components/admin-lte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- page css -->
  @yield('page-css')

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
@include('admin.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
@include('admin.sidebar')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')

  <!-- /.content-wrapper -->

  <!-- Main Footer -->
@include('admin.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{url('bower_components/admin-lte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{url('bower_components/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('bower_components/admin-lte/dist/js/adminlte.min.js')}}"></script>
<!-- DataTables -->
<script src="{{url('bower_components/admin-lte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{url('bower_components/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

@yield('page-script')

<!-- OPTIONAL SCRIPTS -->
<script src="{{url('bower_components/admin-lte/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{url('bower_components/admin-lte/dist/js/demo.js')}}"></script>
<script src="{{url('bower_components/admin-lte/dist/js/pages/dashboard3.js')}}"></script>

</body>
</html>
