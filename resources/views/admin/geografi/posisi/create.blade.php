@extends('admin.app')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>POSISI GEOGRAFI DAN TINGGI GUNUNG API JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="#">Data Geografi</a></li>
              <li class="breadcrumb-item active">Posisi Geografi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <form class="form-horizontal" action="/admin/posisi-geografi/store" method="POST">
        <div class="card">
            <h5 class="card-header">
              Tambah Data Baru
            </h5>
            <div class="card-body">
                @csrf
                <div class="form-group row">
                  <label for="formNamaGunung" class="col-sm-2 col-form-label">Nama Gunung</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formNamaGunung" name="nama_gunung">
                    @if($errors->has('nama_gunung'))
                        <div class="text-danger"><small>{{ $errors->first('nama_gunung') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formTahun" class="col-sm-2 col-form-label">Tahun</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formTahun" name="tahun">
                    @if($errors->has('tahun'))
                        <div class="text-danger"><small>{{ $errors->first('tahun') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formLintangSelatan" class="col-sm-2 col-form-label">LS/BT</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="formLintangSelatan" name="ls" placeholder="Lintang Selatan">
                    @if($errors->has('ls'))
                        <div class="text-danger"><small>{{ $errors->first('ls') }}</small></div>
                    @endif
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="formBujurTimur" name="bt" placeholder="Bujur Timur">
                    @if($errors->has('bt'))
                        <div class="text-danger"><small>{{ $errors->first('bt') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formTinggi" class="col-sm-2 col-form-label">Tinggi Gunung (m)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formTinggi" name="tinggi">
                    @if($errors->has('tinggi'))
                        <div class="text-danger"><small>{{ $errors->first('tinggi') }}</small></div>
                    @endif
                  </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-danger margin-r-5" href="/admin/luas-wilayah">Cancel</a>
                    <button id="btn-save" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
      </form>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection