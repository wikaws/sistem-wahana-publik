@extends('admin.app')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>PROYEK STRATEGIS JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Proyek Strategis</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title">DataTable with default features</h3> -->
              <button type="button" class="btn btn-success btn-lg">+ Add Data</button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Objek</th>
                  <th>Kabupaten/Kota</th>
                  <th>Bidang</th>
                  <th>Tipe</th>
                  <th>Tahun</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Kebun Raya Kuningan</td>
                  <td>Kabupaten Kuningan</td>
                  <td>Destinasi Pariwisata</td>
                  <td>I</td>
                  <td>2019</td>
                  <td>
                    <button type="button" class="btn btn-default"><i class="fas fa-edit"></i></button> | 
                    <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                  </td>
                </tr>
                <tr>
                  <td>Kebun Bawang Panyaweuyan</td>
                  <td>Kabupaten Majalengka</td>
                  <td>Destinasi Pariwisata</td>
                  <td>I</td>
                  <td>2019</td>
                  <td>
                    <button type="button" class="btn btn-default"><i class="fas fa-edit"></i></button> | 
                    <button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                  </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                  <th>Objek</th>
                  <th>Kabupaten/Kota</th>
                  <th>Bidang</th>
                  <th>Tipe</th>
                  <th>Tahun</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('page-script')
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection