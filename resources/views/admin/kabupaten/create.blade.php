@extends('admin.app')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KABUPATEN/KOTA DI JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item active">Kabupaten/Kota</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <form class="form-horizontal" action="/admin/kabupaten/store" method="POST">
        <div class="card">
            <h5 class="card-header">
              Tambah Data Baru
            </h5>
            <div class="card-body">
                @csrf
                <div class="form-group row">
                  <label for="formKategori" class="col-sm-2 col-form-label">Kategori</label>
                  <div class="col-sm-10">
                    <select name="kategori" id="formKategori" class="form-control">
                        <option value="" selected disabled>-- Pilih Kategori --</option>
                        <option value="1">Kabupaten</option>
                        <option value="2">Kota</option>
                    </select>
                    @if($errors->has('kategori'))
                        <div class="text-danger"><small>{{ $errors->first('kategori') }}</small></div>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="formWilayah" class="col-sm-2 col-form-label">Wilayah</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="formWilayah" name="wilayah">
                    @if($errors->has('wilayah'))
                        <div class="text-danger"><small>{{ $errors->first('wilayah') }}</small></div>
                    @endif
                  </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="text-right">
                    <a class="btn btn-danger margin-r-5" href="/admin/kabupaten">Cancel</a>
                    <button id="btn-save" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
      </form>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection