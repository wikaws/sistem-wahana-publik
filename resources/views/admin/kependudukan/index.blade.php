@extends('admin.app')

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show">
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@section('content')
  <div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KEPENDUDUKAN DI JAWA BARAT</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Kependudukan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <a href="{{ url('admin/kependudukan/create') }}" class="btn btn-success btn-lg" role="button">+ Add Data</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kabupaten/Kota</th>
                  <th>Tahun</th>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                  <th>Jumlah Penduduk</th>
                  <th>Rasio Jenis<br> Kelamin</th>
                  <th>Laju Pertumbuhan<br> Penduduk (%)</th>
                  <th>Kepadatan Penduduk<br> Per km<sup>2</sup></th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{ $data->city->name }}</td>
                        <td>{{ $data->year }}</td>
                        <td>{{ $data->male }}</td>
                        <td>{{ $data->female }}</td>
                        <td>{{ $data->population }}</td>
                        <td>{{ $data->sex_ratio }}</td>
                        <td>{{ $data->growth_rate }}</td>
                        <td>{{ $data->population_density }}</td>
                        <td>
                            <a href="/admin/kependudukan/edit/{{ $data->id }}" class="btn btn-default"><i class="fas fa-edit"></i></a> | 
                            <a href="/admin/kependudukan/delete/{{ $data->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('page-script')
<!-- page script -->
<script>
  $(function () {
    $("#dataTable").DataTable({});
  });
</script>
@endsection