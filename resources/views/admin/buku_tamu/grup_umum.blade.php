@extends('admin.app')

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show">
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@section('content')
  <div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DATA BUKU TAMU - ROMBONGAN UMUM</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Dashboard</a></li>
              <li class="breadcrumb-item">Buku Tamu</li>
              <li class="breadcrumb-item active">Rombongan Umum</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama</th>
                  <th>No Telp</th>
                  <th>Email</th>
                  <th>Alamat</th>
                  <th>Umur <5 th</th>
                  <th>Umur 6-12 th</th>
                  <th>Umur 13-20 th</th>
                  <th>Umur 21-30 th</th>
                  <th>Umur 31-50 th</th>
                  <th>Umur >50 th</th>
                  <th>Total Pengunjung</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->phone }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->address }}</td>
                    <td>{{ $data->visitor_age_1 }}</td>
                    <td>{{ $data->visitor_age_2 }}</td>
                    <td>{{ $data->visitor_age_3 }}</td>
                    <td>{{ $data->visitor_age_4 }}</td>
                    <td>{{ $data->visitor_age_5 }}</td>
                    <td>{{ $data->visitor_age_6 }}</td>
                    <td>{{ $data->total_visitor }}</td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('page-script')
<!-- page script -->
<script>
  $(function () {
    $("#dataTable").DataTable({});
  });
</script>
@endsection