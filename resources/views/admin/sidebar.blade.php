  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('images/logo-jabar.png')}}" alt="Jabar Command Center" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Sistem Wahana Publik</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('images/avatar.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Hi, {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{url('admin/dashboard')}}" class="nav-link {{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-header">SEKTOR 1A</li>
          <li class="nav-item has-treeview {{ (request()->is('admin/kabupaten') || request()->is('admin/luas-wilayah') 
          || request()->is('admin/posisi-geografi')  || request()->is('admin/wilayah-administratif') 
          || request()->is('admin/dprd') || request()->is('admin/pns')
           || request()->is('admin/kependudukan') || request()->is('admin/ketenagakerjaan')) ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-folder-open"></i>
              <p>
                Data & Documentation
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{url('admin/kabupaten')}}" class="nav-link {{ (request()->is('admin/kabupaten')) ? 'active' : '' }}">
                  <p>Data Kabupaten/Kota</p>
                </a>
              </li>
              <li class="nav-item {{ (request()->is('admin/luas-wilayah') || request()->is('admin/posisi-geografi')) ? 'menu-open' : '' }}">
                <a href="#" class="nav-link">
                  <p>
                      Data Geografi
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{url('admin/luas-wilayah')}}" class="nav-link {{ (request()->is('admin/luas-wilayah')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Luas Wilayah</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{url('admin/posisi-geografi')}}" class="nav-link {{ (request()->is('admin/posisi-geografi')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Posisi Geografi</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item {{ (request()->is('admin/wilayah-administratif') || request()->is('admin/dprd') || request()->is('admin/pns')) ? 'menu-open' : '' }}">
                <a href="#" class="nav-link">
                  <!-- <i class="far fa-circle nav-icon"></i> -->
                  <p>Data Pemerintahan<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{url('admin/wilayah-administratif')}}" class="nav-link {{ (request()->is('admin/wilayah-administratif')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Wilayah Administratif</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{url('admin/dprd')}}" class="nav-link {{ (request()->is('admin/dprd')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Data DPRD</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{url('admin/pns')}}" class="nav-link {{ (request()->is('admin/pns')) ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Data PNS</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/kependudukan')}}" class="nav-link {{ (request()->is('admin/kependudukan')) ? 'active' : '' }}">
                  <!-- <i class="far fa-circle nav-icon"></i> -->
                  <p>Data Kependudukan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/ketenagakerjaan')}}" class="nav-link {{ (request()->is('admin/ketenagakerjaan')) ? 'active' : '' }}">
                  <!-- <i class="far fa-circle nav-icon"></i> -->
                  <p>Data Ketenagakerjaan</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('admin/vr-contents')) ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-vr-cardboard"></i>
              <p>
                Area VR
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('admin/vr-contents') }}" class="nav-link {{ (request()->is('admin/vr-contents')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Konten VR</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('admin/guest-book*')) ? 'menu-open' : '' }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book-open"></i>
              <p>
                Buku Tamu
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('admin/guest-book/perorangan') }}" class="nav-link {{ (request()->is('admin/guest-book/perorangan')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Perorangan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('admin/guest-book/rombongan-umum') }}" class="nav-link {{ (request()->is('admin/guest-book/rombongan-umum')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Rombongan Umum</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('admin/guest-book/rombongan-instansi') }}" class="nav-link {{ (request()->is('admin/guest-book/rombongan-instansi')) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Rombongan Instansi</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>