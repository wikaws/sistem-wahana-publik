<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guest Book - Jabar Command Center</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style type="text/css">
        /* #results { padding:20px; border:1px solid; background:#ccc; } */
    </style>
</head>
<body>
<div class="container-fluid">
    <br><br>    
    <div class="row row justify-content-center">
        <h1>JCC GUEST BOOK</h1>
    </div>
    <br><br><br>
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div><br><br>
    @endif
    <form action="{{url('guest-book/add')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <h5 class="card-header">Profile Photo</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row justify-content-center">
                                <div id="results"><img src="{{url('images/avatar.png')}}" alt="avatar-placeholder"></div>
                                <input type="hidden" name="image" class="image-tag">
                            </div><br>
                            @if($errors->has('image'))
                                <div class="text-danger"><small>{{ $errors->first('image') }}</small></div>
                            @endif
                            <div class="row justify-content-center">
                                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#exampleModalCenter">
                                Open Camera
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-6">
                <div class="card">
                    <h5 class="card-header">Contact Information</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Full Name</label>
                            <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="John Doe">
                            @if($errors->has('name'))
                                <div class="text-danger"><small>{{ $errors->first('name') }}</small></div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com">
                            @if($errors->has('email'))
                                <div class="text-danger"><small>{{ $errors->first('email') }}</small></div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput3">Phone Number</label>
                            <input type="text" name="phone" class="form-control" id="exampleFormControlInput3" placeholder="+62 896 9438 5796">
                            @if($errors->has('phone'))
                                <div class="text-danger"><small>{{ $errors->first('phone') }}</small></div>
                            @endif
                        </div>
                        <br>
                        <button type="submit" class="btn btn-block btn-dark">Daftar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Take a photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row  justify-content-center">
            <div id="my_camera"></div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="take_snapshot()">Take Picture</button>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
  
    Webcam.attach( '#my_camera' );
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'" width="240"/>';
            $('#exampleModalCenter').modal('hide')
        } );
    }
</script>
</body>
</html>