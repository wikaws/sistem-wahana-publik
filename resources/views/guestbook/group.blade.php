<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Buku Tamu - Jabar Command Center</title>
  <link href="{{ url('css/custom.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <!-- Bootstrap core CSS -->
  <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700&display=swap" rel="stylesheet">

</head>

<body>

  <!-- Page Content -->
  <div class="container pt-5">
    <form action="{{url('guest-book/add-group')}}" method="post">
      @csrf
      <div class="row">
        <div class="col-lg-5 text-center animated fadeInUp slow">
          <div class="card choose-role">
            <h2 class="mt-3 mb-4 animated fadeInUp slower">Ambil Foto</h2>
            <div class="form-group">
              <div class="row justify-content-center">
                <div id="results"><img src="{{ url('images/cam.svg') }}" alt="avatar-placeholder"></div>
                <input type="hidden" name="image" class="image-tag">
              </div><br>
              @if($errors->has('image'))
                  <div class="text-danger"><small>{{ $errors->first('image') }}</small></div>
              @endif
              <div class="row justify-content-center">
                <button type="button" class="mt-3 btn btn-outline-dark" data-toggle="modal" data-target="#modalcamera">
                  Buka Kamera
                </button>
              </div>
            </div>
          </div>
          <a href="{{ url('guest-book') }}" class="btn btn-primary btn-outlined animated fadeInUp slower"><img src="{{ url('images/back.svg') }}" class="img-btn"> Kembali</a>
        </div>
        <div class="col-lg-7 text-center">
        <div class="card choose-role animated fadeInUp slow mb-3">
          <h2 class="mt-3 animated fadeInUp slower">Data Pengunjung</h2>
          <p class="mb-4 animated fadeInUp slow">Ambil foto terlebih dahulu, kemudian lengkapi data berikut</p>

          <nav class="mb-4">
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Rombongan Umum</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Rombongan Instansi</a>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <div class="form-group">
                <label for="exampleFormControlInput1">Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" id="exampleFormControlInput1" placeholder="John Doe">
              </div>
              <div class="form-row">
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput3">Nomor Telepon/Ponsel</label>
                    <input type="text" name="telp" class="form-control" id="exampleFormControlInput3" placeholder="081234xxxxxx">
                  </div>  
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput2">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com">
                  </div>
                </div> 
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput3">Alamat</label>
                <textarea type="text" name="alamat" class="form-control" id="exampleFormControlInput3" placeholder="Alamat lengkap.."></textarea> 
              </div> 
              <div class="form-row">
                <div class="col-12"> 
                  <div class="form-group mb-0">
                    <label for="exampleFormControlInput3">Jumlah Peserta Rombongan (orang)</label>
                  </div> 
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text"><5 th</div>
                      </div>
                      <input type="text" name="umur_1" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div> 
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">6-12 th</div>
                      </div>
                      <input type="text" name="umur_2" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div>  
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">13-20 th</div>
                      </div>
                      <input type="text" name="umur_3" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div> 
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">21-30 th</div>
                      </div>
                      <input type="text" name="umur_4" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div>  
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">31-50 th</div>
                      </div>
                      <input type="text" name="umur_5" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div> 
                </div>
                <div class="col-4"> 
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">> 50 th</div>
                      </div>
                      <input type="text" name="umur_6" class="form-control" id="inlineFormInputGroup" placeholder="Total..">
                    </div>
                  </div>  
                </div>
              </div> 
              <br>
              <input type="hidden" name="type" value="umum">
              <button type="submit" class="btn btn-block btn-dark btn-submit">Daftar</button>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <div class="form-group">
                <label for="exampleFormControlInput1">Nama Lengkap</label>
                <input type="text" name="nama_instansi" class="form-control" id="exampleFormControlInput1" placeholder="John Doe">
              </div>
              <div class="form-row">
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput3">Nomor Telepon/Ponsel</label>
                    <input type="text" name="telp_instansi" class="form-control" id="exampleFormControlInput3" placeholder="081234xxxxxx">
                  </div>  
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput2">Email</label>
                    <input type="email" name="email_instansi" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com">
                  </div>
                </div> 
              </div>
              <div class="form-row">
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput3">Instansi</label>
                    <input type="text" name="instansi" class="form-control" id="exampleFormControlInput3" placeholder="Instansi atau Dinas">
                  </div>  
                </div>
                <div class="col">
                  <div class="form-group">
                    <label for="exampleFormControlInput2">Jumlah Pengunjung</label>
                    <input type="text" name="total_pengunjung" class="form-control" id="exampleFormControlInput2" placeholder="Jumlah (orang)">
                  </div>
                </div> 
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput3">Keperluan</label>
                <textarea type="text" name="keperluan" class="form-control" id="exampleFormControlInput3" placeholder="Keperluan untuk.."></textarea> 
              </div> 
              <br>
              <input type="hidden" name="type_instansi" value="instansi">
              <button type="submit" class="btn btn-block btn-dark btn-submit">Daftar</button>
            </div>
          </div>
        
        
        </div>
      </div>
      </div>
    </form>
  </div>

  <!-- Modal Camera -->
  <div class="modal fade" id="modalcamera" tabindex="-1" role="dialog" aria-labelledby="modalcameralabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Take a photo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row  justify-content-center">
              <div id="my_camera" style="width: 490px; height: 390px;"><div></div><video autoplay="autoplay" style="width: 490px; height: 390px;"></video></div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="take_snapshot()">Take Picture</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
  <!-- <script src="vendor/jquery/jquery.slim.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <script type="text/javascript">
    $(".xxl-btn").hover(function (e) {
        $(this).addClass('animated pulse');
    });

    $(".xxl-btn").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
        $(this).removeClass('animated pulse');
    });

    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
  
    Webcam.attach( '#my_camera' );
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'" width="240"/>';
            $('#modalcamera').modal('hide')
        } );
    }
  </script>
</body>

</html>
