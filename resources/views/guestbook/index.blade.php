<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Buku Tamu - Jabar Command Center</title>
  <link href="{{ url('css/custom.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <!-- Bootstrap core CSS -->
  <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700&display=swap" rel="stylesheet">

  <style>
    .toast-header {
      color: #000 !important;
    }
    .toast-body {
      color: #000 !important;
    }
  </style>

</head>

<body>

  <!-- Page Content -->
  <div class="container pt-5">
    <div class="row">
      <div class="col-lg-4 text-center">
        <div class="logo-jcc animated pulse infinite slower">
          <img src="images/logo-jcc.svg">
        </div>
      </div>
      <div class="col-lg-8 text-center">
        <div class="card choose-role animated fadeInUp slow">
          <h2 class="mt-5 animated fadeInUp slower">Selamat datang di <br>Jabar Command Center</h2>
          <p class="animated fadeInUp slow">Pilih tipe kunjungan anda sebelum mendaftar sebagai tamu</p>
          <div class="row">
            <div class="col-md-6">
              <a href="guest-book/group" class="xxl-btn btn">
                <img src="images/group.svg" class="xxl-img mt-4">
                ROMBONGAN
              </a>
            </div>
            <div class="col-md-6">
              <a href="guest-book/personal" class="xxl-btn btn">
                <img src="images/individuals.svg" class="xxl-img mt-4">
                PERORANGAN
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="10000">
      <div class="toast-body"> -->
        <!-- {{ session()->get('success') }} -->
        <!-- Hello this is toast!
      </div>
    </div> -->

@if(session()->has('success'))
  <div class="modal fade" id="modalToast" tabindex="-1" role="dialog" aria-labelledby="modaltoastlabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Success</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            {{ session()->get('success') }}
        </div>
      </div>
    </div>
  </div>
@endif

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <!-- <script src="{{asset('vendor/jquery/jquery.slim.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script> -->
  <script type="text/javascript">
    // $('.toast').toast('show');
    $('#modalToast').modal('show')
    $(".xxl-btn").hover(function (e) {
        $(this).addClass('animated pulse');
    });

    $(".xxl-btn").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
        $(this).removeClass('animated pulse');
    });
  </script>
</body>

</html>
