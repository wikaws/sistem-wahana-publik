<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Visualization - JCC</title>

  <!-- Bootstrap core CSS -->
  <link href="wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="wahana-publik-new/custom/css/custom.css" rel="stylesheet">

</head>

<body>


  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="div-panel">
          <h2 class="mt-4">Selamat datang di Wahana Visualisasi Data</h2>
          <p class="subtitle mb-5">Pilih menu berikut ini untuk melanjutkan</p>
          <div class="row">
            <div class="col-1"></div>
            <div class="col-5">
              <a href="{{ route('jda-kategori') }}" class="img-icon mt-3">
                <img src="wahana-publik-new/img/present.svg">
                Jabar Dalam Angka
              </a>
            </div>
            <div class="col-5">
              <a href="#" class="img-icon mt-3">
                <img src="wahana-publik-new/img/grafik.svg">
                Grafik & Statistik
              </a>
            </div>
            <div class="col-1"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="wahana-publik-new/vendor/jquery/jquery.slim.min.js"></script>
  <script src="wahana-publik-new/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
