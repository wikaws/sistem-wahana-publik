<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Visualization - JCC</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">


</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg fixed-bottom navbar-dark bg-dark static-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('index-home') }}"><img src="{{asset('wahana-publik-new/img/back.svg')}}" class="back-ico"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">
              <img src="{{asset('wahana-publik-new/img/present.svg')}}" class="icon-nav">
              Jabar Dalam Angka
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <img src="{{asset('wahana-publik-new/img/grafik.svg')}}" class="icon-nav">
              Grafik dan Statistik
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="div-panel-wide">
          <h2 class="mt-4">JAWA BARAT DALAM ANGKA</h2>
          <p class="subtitle mb-5">Pilih kategori berikut ini untuk melanjutkan</p>
          <div class="row sub-menu">
            <div class="col-1"></div>
            <div class="col">
              <a href="{{ route('jda-geo') }}" class="img-icon mt-3">
                <img src="{{asset('wahana-publik-new/img/icon-geo.svg')}}">
                Data Geografi & Iklim
              </a>
            </div>
            <div class="col">
              <a href="{{ route('jda-gov') }}" class="img-icon mt-3">
                <img src="{{asset('wahana-publik-new/img/icon-gov.svg')}}">
                Data Pemerintahan
              </a>
            </div>
            <div class="col">
              <a href="{{ route('jda-mass') }}" class="img-icon mt-3">
                <img src="{{asset('wahana-publik-new/img/icon-mass.svg')}}">
                Data Kependudukan & Ketenagakerjaan
              </a>
            </div>
            <div class="col-1"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</body>

</html>
