<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Visualization - JCC</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg fixed-bottom navbar-dark bg-dark static-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('jda-kategori') }}"><img src="{{asset('wahana-publik-new/img/back.svg')}}" class="back-ico"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <nav aria-label="Page navigation example" class="year">
        <ul class="pagination justify-content-center">
          <p class="title-year">Pilih Tahun</p>
          <li class="page-item active"><a class="page-link" href="#">2018</a></li>
          <li class="page-item"><a class="page-link" href="#">2019</a></li>
          <li class="page-item"><a class="page-link" href="#">2020</a></li>
        </ul>
      </nav>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-geo') }}">
              <img src="{{asset('wahana-publik-new/img/icon-geo.svg')}}" class="icon-nav">
              Data Geografi & Iklim
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-gov') }}">
              <img src="{{asset('wahana-publik-new/img/icon-gov.svg')}}" class="icon-nav">
              Data Pemerintahan
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">
              <img src="{{asset('wahana-publik-new/img/icon-mass.svg')}}" class="icon-nav">
              Data Kependudukan & Ketenagakerjaan
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container-fluid content gov">
    <div class="row">
      <div class="col-12">
        <div class="head-title">
          <p>Data Kependudukan & Ketenagakerjaan</p>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-jum-penduduk">
          <div class="value-box">
            <p class="title-1">Jumlah Penduduk di Jawa Barat</p>
            <p class="value">48.037.827</p>
          </div>
        </div>
        <div class="div-panel-rasio-kel">
          <div class="value-box">
            <p class="title-1">Rasio Jenis Kelamin</p>
            <p class="value">1,03</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-kepadatan">
          <div class="value-box">
            <p class="title-1">Kepadatan Penduduk per km²</p>
            <p class="value">1.358</p>
          </div>
        </div>
        <div class="div-panel-pengangguran">
          <div class="value-box">
            <p class="title-1">Tingkat Pengangguran</p>
            <p class="value">8,22%</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-laju">
          <div class="value-box">
            <p class="title-1">Laju Pertumbuhan Penduduk</p>
            <p class="value">1,39</p>
          </div>
        </div>
        <div class="div-panel-part-kerja">
          <div class="value-box">
            <p class="title-1">Tingkat Partisipasi Kerja</p>
            <p class="value">63,34%</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-angkerja">
          <div class="value-box">
            <p class="title-1">JUmlah Angkatan Kerja Tahun 2017</p>
            <p class="value">3.291</p>
          </div>
        </div>
        <div class="div-panel-penduduk-besar">
          <div class="value-box">
            <p class="title-1">Jumlah Penduduk Terbanyak</p>
            <p class="value">5.715.009 jiwa</p>
            <p class="additional">[ Kabupaten Bogor ]</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</body>

</html>
