<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Visualization - JCC</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg fixed-bottom navbar-dark bg-dark static-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('jda-kategori') }}"><img src="{{asset('wahana-publik-new/img/back.svg')}}" class="back-ico"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <nav aria-label="Page navigation example" class="year">
        <ul class="pagination justify-content-center">
          <p class="title-year">Pilih Tahun</p>
          <li class="page-item active"><a class="page-link" href="#">2018</a></li>
          <li class="page-item"><a class="page-link" href="#">2019</a></li>
          <li class="page-item"><a class="page-link" href="#">2020</a></li>
        </ul>
      </nav>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">
              <img src="{{asset('wahana-publik-new/img/icon-geo.svg')}}" class="icon-nav">
              Data Geografi & Iklim
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-gov') }}">
              <img src="{{asset('wahana-publik-new/img/icon-gov.svg')}}" class="icon-nav">
              Data Pemerintahan
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-mass') }}">
              <img src="{{asset('wahana-publik-new/img/icon-mass.svg')}}" class="icon-nav">
              Data Kependudukan & Ketenagakerjaan
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container-fluid content">
    <div class="row">
      <div class="col-12">
        <div class="head-title">
          <p>Data Geografi Jawa Barat</p>
        </div>
      </div>
      <div class="col-6 text-center">
        <div class="div-map">
          <img src="{{asset('wahana-publik-new/img/map-jabar.svg')}}" class="map-img">
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-luas">
          <div class="value-box">
            <p class="title-1">Luas Wilayah Jawa Barat</p>
            <p class="value">35.378 km²</p>
          </div>
        </div>
        <div class="div-panel-luas-kota-besar">
          <div class="value-box">
            <p class="title-1">Kota/Kabupaten terluas</p>
            <p class="value">4.145,70 km²</p>
            <p class="additional">[ Kabupaten Sukabumi ]</p>
          </div>
        </div>
        <div class="div-panel-luas-kota-kecil">
          <div class="value-box">
            <p class="title-1">Kota/Kabupaten tersempit</p>
            <p class="value">37,36 km²</p>
            <p class="additional">[ Kota Cirebon ]</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-kota">
          <div class="value-box2">
            <p class="title-1">Jumlah Kota di Jawa Barat</p>
            <p class="value">9 kota</p>
          </div>
        </div>
        <div class="div-panel-kab">
          <div class="value-box2">
            <p class="title-1">Jumlah Kabupaten di Jawa Barat</p>
            <p class="value">18 kabupaten</p>
          </div>
        </div>
        <div class="div-panel-gunung">
          <div class="value-box">
            <p class="title-1">Gunung tertinggi</p>
            <p class="value">3.078 mdpl</p>
            <p class="additional">[ Gunung Ciremai ]</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('wahana-publik-new/vendor/jquery/jquery.slim.min.js')}}"></script>
  <script src="{{asset('wahana-publik-new/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
