<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Visualization - JCC</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg fixed-bottom navbar-dark bg-dark static-top">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{ route('jda-kategori') }}"><img src="{{asset('wahana-publik-new/img/back.svg')}}" class="back-ico"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <nav aria-label="Page navigation example" class="year">
        <ul class="pagination justify-content-center">
          <p class="title-year">Pilih Tahun</p>
          <li class="page-item active"><a class="page-link" href="#">2018</a></li>
          <li class="page-item"><a class="page-link" href="#">2019</a></li>
          <li class="page-item"><a class="page-link" href="#">2020</a></li>
        </ul>
      </nav>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-geo') }}">
              <img src="{{asset('wahana-publik-new/img/icon-geo.svg')}}" class="icon-nav">
              Data Geografi & Iklim
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">
              <img src="{{asset('wahana-publik-new/img/icon-gov.svg')}}" class="icon-nav">
              Data Pemerintahan
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('jda-mass') }}">
              <img src="{{asset('wahana-publik-new/img/icon-mass.svg')}}" class="icon-nav">
              Data Kependudukan & Ketenagakerjaan
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container-fluid content gov">
    <div class="row">
      <div class="col-12">
        <div class="head-title">
          <p>Data Pemerintahan</p>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-dprd-laki">
          <div class="value-box">
            <p class="title-1">Anggota DPRD Laki-laki</p>
            <p class="value">78</p>
          </div>
        </div>
        <div class="div-panel-dprd-per">
          <div class="value-box">
            <p class="title-1">Anggota DPRD Perempuan</p>
            <p class="value">22</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-pns-laki">
          <div class="value-box2">
            <p class="title-1">Pegawai Negeri Sipil Laki-laki</p>
            <p class="value">167.144</p>
          </div>
        </div>
        <div class="div-panel-pns-per">
          <div class="value-box2">
            <p class="title-1">Pegawai Negeri Sipil Perempuan</p>
            <p class="value">164.183</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-kotaan">
          <div class="value-box">
            <p class="title-1">Jumlah Perkotaan</p>
            <p class="value">2.672</p>
          </div>
        </div>
        <div class="div-panel-camatan">
          <div class="value-box">
            <p class="title-1">Jumlah Kecamatan</p>
            <p class="value">627</p>
          </div>
        </div>
      </div>
      <div class="col-3 text-center">
        <div class="div-panel-desaan">
          <div class="value-box">
            <p class="title-1">Jumlah Pedesaan</p>
            <p class="value">3.291</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <link href="{{asset('wahana-publik-new/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('wahana-publik-new/custom/css/custom.css')}}" rel="stylesheet">

</body>

</html>
