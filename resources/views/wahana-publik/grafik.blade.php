<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('wahana-publik/favicon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('wahana-publik/favicon.png')}}">

    <title>Sistem Wahana Publik</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="wahana-publik/js/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
    <link href="wahana-publik/js/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
</head>

<style>
    body,html{
        height: 100%;
        color:white;
    }
    h1{
        text-shadow: 2px 2px 5px #f63b8d;
    }
    .wrapper{
        /* min-height: 30rem; */
        position: relative;
        display: table;
        width: 100%;
        height: 100%;
        padding-top: 8rem;
        padding-bottom: 8rem;
        /* background-color: blanchedalmond; */
        background: url("wahana-publik/img/bg-holo-vr.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .carousel-item{
        margin:auto;
    }
    .carousel-item a{
        color:#00FFFF !important;
        margin-top:10px;
    }
    .carousel-item a:hover{
        text-decoration:none;
    }
    .fas{
        color: black;
        font-size: 5em;
    }
    .menu-circle{
        cursor:pointer;
    }
    .menu-img-circle{
        cursor:pointer;
    }
    .menu-icon{
        position: absolute;
        width: 150px;
        top: 57px;
        left: 477px;
        cursor:pointer;
    }
    .menu-arrow{
        width:75px;
    }

</style>

<body id="page-top">


    <div class="d-flex p-auto wrapper">
        <div class="container text-center my-auto">
            <center>
                <h1>SELAMAT DATANG DI WAHANA PUBLIK INTERAKTIF</h1>
            </center>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href="{{ route('grafik') }}">
                            <div class="menu-cirle">
                                <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                                <img src="wahana-publik/assets/ic_task_holo.png" class="menu-icon" alt="">
                                <h4>Grafik Dan Statistik</h4>
                            </div>
                        </a>
                        <!-- <img src="wahana-publik/img/la.jpg" class="d-block w-50" alt="..."> -->
                    </div>
                    <div class="carousel-item">
                        <a href="{{ route('jabar_angka') }}">
                            <div class="menu-cirle">
                                <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                                <img src="wahana-publik/assets/ic_map_location.png" class="menu-icon" alt="">
                                <h4>Jabar Dalam Angka</h4>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="carousel-item">
                        <div class="menu-cirle">
                            <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                            <img src="wahana-publik/assets/ic_task_holo.png" class="menu-icon" alt="">
                        </div>
                    </div> -->
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
                    <!-- <span class="sr-only">Previous</span> -->
                    <!-- <i class="fas fa-caret-left"></i> -->
                    <img src="wahana-publik/assets/ic_back_holo.png" class="menu-arrow" alt="">
                    
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
                    <!-- <span class="sr-only">Next</span> -->
                    <!-- <i class="fas fa-caret-right"></i> -->
                    <img src="wahana-publik/assets/ic_next_holo.png" class="menu-arrow" alt="">
                </a>
            </div>
        </div>
    <div class="overlay"></div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>

  <!-- Plugin JavaScript -->
  <script src="wahana-publik/js/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
