<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('wahana-publik/favicon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('wahana-publik/favicon.png')}}">

    <title>Sistem Wahana Publik</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="wahana-publik/js/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
    <link href="wahana-publik/js/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
</head>

<style>
    body,html{
        height: 100%;
        color:white;
    }
    h1{
        text-shadow: 2px 2px 5px #f63b8d;
        position:absolute;
        top:50px;
        left:28%;
    }
    .wrapper{
        /* min-height: 30rem; */
        position: relative;
        display: table;
        width: 100%;
        height: 100%;
        padding-top: 8rem;
        padding-bottom: 8rem;
        /* background-color: blanchedalmond; */
        background: url("wahana-publik/img/bg-holo-vr.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .content-fill{
        margin-top:50px;
    }

    .menu-circle{
        cursor:pointer;
    }
    .menu-img-circle{
        cursor:pointer;
    }
    .menu-icon{
        position: absolute;
        width: 100px;
        top: 85px;
        cursor:pointer;
        left: 145px;
    }
    .menu-home a{
        color:#00FFFF !important;
    }
    .menu-home a:hover{
        text-decoration:none;
    }
    .menu-home span{
        font-size: 3em;
        margin-top: 30px;
        position: absolute;
        top: 75px;
        left: 140px;
        font-weight: 900;
        background: -webkit-linear-gradient(#fff, #00FFFF);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
    .btn-back{
        position: absolute;
        top: 50px;
        width: 120px;
        height:auto;
        left: 25px;
    }
    .btn-back-word{
        position:absolute;
        left: 61px;
        top: 57px;
        color:#00FFFF !important;
    }
    
    /* #menu-jabar-angka{
        left:20px;
    }
    #menu-grafik-statistik{
        left:115px;
    }
    #menu-proyek-startegis{
        left:20px;
    } */
    

</style>
<script>
function goBack() {
  window.history.back();
}
</script>
<body id="page-top">


    <div class="d-flex p-auto wrapper">
        <div class="container fluid">
            <div class="row" onclick="goBack()">
                <img src="wahana-publik/assets/btn_back_holo.png" class="btn-back" alt="" srcset="">
                <span class="btn-back-word">
                    Kembali
                </span>
            </div>
            <div class="container text-center my-auto">
            <h1>JABAR DALAM ANGKA PADA TAHUN</h1>
            <div class="row content-fill">
                <div class="col-xl menu-home">
                    <a href="{{ url('jabar-angka/2017') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <!-- <img src="wahana-publik/assets/ic_page_holo.png" class="menu-icon" id="menu-jabar-angka" alt=""> -->
                        <span>2017</span>
                    </a>
                </div>
                <div class="col-xl menu-home">
                    <a href="{{ url('jabar-angka/2018') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <!-- <img src="wahana-publik/assets/grafik.png" class="menu-icon" id="menu-grafik-statistik" alt=""> -->
                        <span>2018</span>
                    </a>
                </div>
                <div class="col-xl menu-home">
                    <a href="{{ url('jabar-angka/2019') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <!-- <img src="wahana-publik/assets/proyek-strategis.png" class="menu-icon" id="menu-proyek-strategis" alt=""> -->
                        <span>2019</span>
                    </a>
                </div>
            </div>
        </div>
        </div>
        
    <div class="overlay"></div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>

  <!-- Plugin JavaScript -->
  <script src="wahana-publik/js/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
