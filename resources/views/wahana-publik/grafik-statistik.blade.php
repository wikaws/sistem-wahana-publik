<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('wahana-publik/favicon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('wahana-publik/favicon.png')}}">

    <title>Sistem Wahana Publik</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="wahana-publik/js/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
    <link href="wahana-publik/js/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
</head>

<style>
    body,html{
        height: 100%;
        color:white;
    }
    h1{
        text-shadow: 2px 2px 5px #f63b8d;
        position:absolute;
        top:50px;
        left:28%;
    }
    .wrapper{
        position: relative;
        width: 100%;
        height: 100%;
        padding: 2rem 0rem;
        background: url("wahana-publik/img/bg-holo-vr.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .btn-back{
        width:120px;
    }
    .btn-back-word{
        position:absolute;
        left: 85px;
        top: 7px;
        color:#00FFFF !important;
        font-weight:600;
    }
    .title-info{
        /* font-weight:bold; */
    }
    

</style>
<script>
function goBack() {
  window.history.back();
}
</script>

<body id="page-top">


    <div class="d-flex p-auto wrapper">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-4 pl-5 action-back" onclick="goBack()">
                    <img src="wahana-publik/assets/btn_back_holo.png" class="btn-back" alt="" srcset="">
                    <span class="btn-back-word">
                        Kembali
                    </span>
                </div>
                <div class="col-4 text-right title-info pr-5">
                    <h5><b>DETAIL GRAFIK & STATISTIK</b></h5>
                </div>
            </div>
        </div>
    <div class="overlay"></div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>

  <!-- Plugin JavaScript -->
  <script src="wahana-publik/js/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
