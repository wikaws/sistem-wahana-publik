<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('wahana-publik/favicon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('wahana-publik/favicon.png')}}">

    <title>Sistem Wahana Publik</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="wahana-publik/js/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css"> -->
    <link href="wahana-publik/js/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
</head>

<style>
    body,html{
        height: 100%;
        color:white;
    }
    h1{
        text-shadow: 2px 2px 5px #f63b8d;
        position:absolute;
        top:50px;
        left:20%;
    }
    .wrapper{
        /* min-height: 30rem; */
        position: relative;
        display: table;
        width: 100%;
        height: 100%;
        padding-top: 8rem;
        padding-bottom: 8rem;
        /* background-color: blanchedalmond; */
        background: url("wahana-publik/img/bg-holo-vr.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .fas{
        color: black;
        font-size: 5em;
    }

    .menu-circle{
        cursor:pointer;
    }
    .menu-img-circle{
        cursor:pointer;
    }
    .menu-icon{
        position: absolute;
        width: 100px;
        top: 85px;
        cursor:pointer;
        left: 145px;
    }
    .menu-home a{
        color:#00FFFF !important;
    }
    .menu-home a:hover{
        text-decoration:none;
    }
    .menu-home h4{
        margin-top:30px;
    }
    
    /* #menu-jabar-angka{
        left:20px;
    }
    #menu-grafik-statistik{
        left:115px;
    }
    #menu-proyek-startegis{
        left:20px;
    } */
    

</style>

<body id="page-top">


    <div class="d-flex p-auto wrapper">
        <div class="container text-center my-auto">
            <h1>SELAMAT DATANG DI WAHANA PUBLIK INTERAKTIF</h1>
            <div class="row">
                <div class="col-xl menu-home">
                    <a href="{{ route('jabar_angka') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <img src="wahana-publik/assets/ic_page_holo.png" class="menu-icon" id="menu-jabar-angka" alt="">
                        <h4>Jabar Dalam Angka</h4>
                    </a>
                </div>
                <div class="col-xl menu-home">
                    <a href="{{ route('grafik') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <img src="wahana-publik/assets/grafik.png" class="menu-icon" id="menu-grafik-statistik" alt="">
                        <h4>Grafik Dan Statistik</h4>
                    </a>
                </div>
                <div class="col-xl menu-home">
                    <a href="{{ route('proyek_strategis') }}">
                        <img src="wahana-publik/assets/circle.png" class="menu-img-circle" alt="">
                        <img src="wahana-publik/assets/proyek-strategis.png" class="menu-icon" id="menu-proyek-strategis" alt="">
                        <h4>Proyek Strategis</h4>
                    </a>
                </div>
            </div>
        </div>
    <div class="overlay"></div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>

  <!-- Plugin JavaScript -->
  <script src="wahana-publik/js/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
